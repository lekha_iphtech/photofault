//
//  HTStoreObserver.h
//  HTStoreKit
//
//  Created by Bruce Mackintosh on 16/08/2011.
//  Copyright 2011 Private. All rights reserved.

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

//#import "ASIHTTPRequest.h"

@interface HTStoreObserver : NSObject<SKPaymentTransactionObserver,UIAlertViewDelegate> {
	SKPaymentTransaction *unverifiedTransaction;	
	NSOperationQueue *queue;
    NSArray *transactionsSaved;
    
}

@property(nonatomic, retain) NSOperationQueue *queue;

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions;

- (void) failedTransaction: (SKPaymentTransaction *)transaction;
- (void) completeTransaction: (SKPaymentTransaction *)transaction;
- (void) restoreTransaction: (SKPaymentTransaction *)transaction;
- (void) verifyTransaction:(SKPaymentTransaction *)transaction;

//-(void)transactionCanceled;
@end
