
//
//  StoreManager.m
//  HTStoreKit
//
//  Created by Bruce Mackintosh on 16/08/2011.
//  Copyright 2011 Private. All rights reserved.

#import "HTStoreManager.h"

@interface HTStoreManager (PrivateMethods)

- (void) buyLicense:(NSString*) featureId;

//- (void) requestProductData:(NSArray *)productArray;
- (NSMutableArray*) purchasableObjectsDescription;
- (void) activeStoreKitAlert;
- (void) showTransactionErrorAlert;
- (void) errorAlert:(NSError*)error;
- (void) canMakePaymentsAlert;
-(UIAlertView *)errorAlert;

@end

#define ERROR_ALERT_TAG 101

@implementation HTStoreManager

@synthesize purchasableObjects = _purchasableObjects;
@synthesize storeObserver = _storeObserver;
@synthesize productsRequest;
@synthesize verfiyDictionary;


static __weak id<HTStoreKitDelegate> _delegate;
static HTStoreManager * _sharedStoreManager;


#pragma mark Delegates

+ (id)delegate {
	
    return _delegate;
}

+ (void)setDelegate:(id)newDelegate {
	
    _delegate = newDelegate;	
}

#pragma mark Singleton Methods

+ (HTStoreManager*)sharedManager 
{
	@synchronized(self) {
		
        if (_sharedStoreManager == nil) {
            
            _sharedStoreManager = [[self alloc] init];		
			_sharedStoreManager.purchasableObjects = [NSMutableArray array];
           
			_sharedStoreManager.storeObserver = [[HTStoreObserver alloc] init];
			[[SKPaymentQueue defaultQueue] addTransactionObserver:_sharedStoreManager.storeObserver];
        }
    }
    return _sharedStoreManager;
}


+ (id)allocWithZone:(NSZone *)zone

{	
    @synchronized(self) {
		
        if (_sharedStoreManager == nil) {
			
            _sharedStoreManager = [super allocWithZone:zone];			
            return _sharedStoreManager;  
        }
    }
	
    return nil; 
}


- (id)copyWithZone:(NSZone *)zone
{
    return self;	
}


#pragma mark -
#pragma mark Internal MKStoreKit functions

- (void) restorePreviousTransactions{
    
	if ([SKPaymentQueue canMakePayments]){
		if(!isStoreKitActive){
			isStoreKitActive = YES;
			[[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
		}
		else{
			[self activeStoreKitAlert];
		}
	}
	else{

        NSError *error = [[NSError alloc] initWithDomain:@"Transaction failed" code:6121 userInfo:nil];
		if([_delegate respondsToSelector:@selector(paymentQueueRestoreCompletedTransactionsFailed:)]){
			[_delegate paymentQueueRestoreCompletedTransactionsFailed:error];

        }
	}
}


+(NSDictionary*) storeKitItems
{
    return [NSDictionary dictionaryWithContentsOfFile:
            [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:
             @"MKStoreKitConfigs.plist"]];
}

- (void) requestProductData:(NSString *)productId
{
	
    NSMutableArray *productsArray = [NSMutableArray array];
   [productsArray addObject:productId];
    
	NSMutableSet *objects = [NSMutableSet setWithArray:productsArray];
	SKProductsRequest *request= [[SKProductsRequest alloc] initWithProductIdentifiers:objects];
	self.productsRequest = request;
	request.delegate = self;
	[request start];
}

- (void)requestProductsData:(NSArray *)productIdArray
{
    
    NSMutableArray *productsArray = [NSMutableArray array];
    [productsArray addObjectsFromArray:productIdArray];
    
    NSMutableSet *objects = [NSMutableSet setWithArray:productsArray];
    SKProductsRequest *request= [[SKProductsRequest alloc] initWithProductIdentifiers:objects];
    self.productsRequest = request;
    request.delegate = self;
    [request start];
    
}


- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
	[self.purchasableObjects removeAllObjects];
	[self.purchasableObjects addObjectsFromArray:response.products];
#if DEBUG
    for(int i=0;i<[self.purchasableObjects count];i++)
    {
        product = [self.purchasableObjects objectAtIndex:i];
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [numberFormatter setLocale:product.priceLocale];
        NSString *formattedString = [numberFormatter stringFromNumber:product.price];
        NSLog(@"Feature: %@, Cost: %f, ID: %@, priceLocale: %@, formattedString: %@",[product localizedTitle],  [[product price] doubleValue], [product productIdentifier] , product.priceLocale, formattedString);
        
        [[NSUserDefaults standardUserDefaults]setObject:formattedString forKey:@"currentCountry"];
        
        if([[NSUserDefaults standardUserDefaults]boolForKey:@"viewControl"])
        {
          [[NSUserDefaults standardUserDefaults]setObject:formattedString forKey:@"currentcurrency"];
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
    for(NSString *invalidProduct in response.invalidProductIdentifiers)
    {
        NSLog(@"Problem in iTunes connect configuration for product: %@", invalidProduct);
        [_delegate productFetchFail:nil];
    }
#endif

	isProductsAvailable = YES;
	
	NSMutableArray *productArray = [self purchasableObjectsDescription];
	
	if([_delegate respondsToSelector:@selector(productFetchComplete:)]){
		[_delegate productFetchComplete:productArray];
	}
	
	isStoreKitActive = NO;
}

-(void)requestDidFinish:(SKRequest *)request
{
    SKProduct *book;
    
    if([self.purchasableObjects count]>0)
    book = [self.purchasableObjects objectAtIndex:0];
    else
        return;
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:book.priceLocale];
    
   /* NSLocale* storeLocale = book.priceLocale;
    NSString *storeCountry = (NSString*)CFLocaleGetValue((CFLocaleRef)storeLocale, kCFLocaleCountryCode);*/
    
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error{
    
    if([_delegate respondsToSelector:@selector(productFetchFail:)]){
        
        NSLog(@"request faild with error----%@", error);
		[_delegate productFetchFail:error];	
	} 
    
}



- (NSMutableArray*) purchasableObjectsDescription{
	
	NSMutableArray *productDescriptions = [[NSMutableArray alloc] initWithCapacity:[self.purchasableObjects count]];
	
	for(int i = 0;i < [self.purchasableObjects count]; i++){
		
		SKProduct *product_1 = [self.purchasableObjects objectAtIndex:i];
		
		[productDescriptions addObject: product_1];
	}
    
	//[productDescriptions autorelease];
	return productDescriptions;
	
	return nil;
}


-(NSDictionary*)verfiyDictionary{
	return verfiyDictionary;	
}


-(void)verifyLicenseWithDictionary:(NSDictionary*)dict{	
	NSLog(@"%@, %@",[self class], NSStringFromSelector(_cmd));
	
	self.verfiyDictionary = dict;	
	
    
}

- (void) buyLicenseWithDictionary:(NSDictionary*)dict
{
    
	self.verfiyDictionary = dict;
    
	if ([SKPaymentQueue canMakePayments]){
		if(!isStoreKitActive){
			isStoreKitActive = YES;
			
            for(int i=0;i<[self.purchasableObjects count];i++){
                
                product = [self.purchasableObjects objectAtIndex:i];
                NSLog(@"product %@", [product productIdentifier]);
                
                if ([[product productIdentifier] isEqualToString:[self.verfiyDictionary objectForKey:LICENSETYPE_KEY]]) {
                    break;
                }
            }
            if (product) {
                 //[self buyLicenseProduct:product];
            }

		}
	}
	else{		
#if TARGET_IPHONE_SIMULATOR
		[self.storeObserver verifyTransaction:nil];
#else	
		[self canMakePaymentsAlert];
#endif
	}
}



-(void)buyLicenseProduct:(SKProduct *) productSelected
{
    if ([SKPaymentQueue canMakePayments])
    {
		SKPayment *payment = [SKPayment paymentWithProduct:productSelected];
		[[SKPaymentQueue defaultQueue] addPayment:payment];
	}
	else{
		[self canMakePaymentsAlert];
	}
    
}


-(BOOL)isStoreKitActive{
	return isStoreKitActive;
}


#pragma mark -
#pragma mark In-App purchases callbacks





-(void) provideContent: (NSMutableDictionary*) dictionary
{
    
      
	isStoreKitActive = NO;
	if([_delegate respondsToSelector:@selector(productPurchased:)]){
		[_delegate productPurchased:dictionary];	
	}
    
}

- (void) transactionCanceled: (NSString *)clipId{
    
    
    
    isStoreKitActive = NO;
    
   
	if([_delegate respondsToSelector:@selector(transactionCanceled:)]){
		[_delegate transactionCanceled:clipId];
	}
}


- (void) failedTransaction: (NSDictionary*) dictionary error:(NSError*)error{
	
	NSLog(@"Error = %@", error);
	if(!error){
		error = [[NSError alloc] initWithDomain:@"Transaction failed" code:6121 userInfo:nil];
	}
	isStoreKitActive = NO;
	
	if(!errorAlert){
		[self errorAlert:error];
	}
    
	if([_delegate respondsToSelector:@selector(transactionCanceled:)]){
		NSString *videoCode = [dictionary objectForKey:@"videoCode"];
		[_delegate transactionCanceled:videoCode];
	}
}

-(void)paymentQueueRestoreCompletedTransactionsFailed:(NSError *)error{
    
	isStoreKitActive = NO;
	if([_delegate respondsToSelector:@selector(paymentQueueRestoreCompletedTransactionsFailed:)]){
		[_delegate paymentQueueRestoreCompletedTransactionsFailed:error];
    }
}

-(void)paymentQueueRestoreCompletedTransactionsFinished{
	isStoreKitActive = NO;
    if([_delegate respondsToSelector:@selector(paymentQueueRestoreCompletedTransactionsFinished)]){
        
		[_delegate paymentQueueRestoreCompletedTransactionsFinished];
    }
}



#pragma mark -
#pragma mark Alerts

-(void)showTransactionErrorAlert{
	
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"An error occured during your transaction. Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    //[alert release];
    
    
}

-(void)errorAlert:(NSError*)error{
	
	if(!errorAlert){
		NSString *message = nil;
		if(error){
			message = [NSString stringWithFormat:@"%@.", [error localizedDescription]];
		}
		else{
			message = @"An error occured during your transaction";
		}
        
        errorAlert = [[UIAlertView alloc]initWithTitle:@"Alert" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        errorAlert.tag = ERROR_ALERT_TAG;
        [errorAlert show];
       // [errorAlert release];
        
        
       
        
       // if (status == 1) {
            //errorAlert = nil;
            //[HTStoreManager setDelegate: nil];
        //}
		
	}
}

-(UIAlertView *)errorAlert{
    return errorAlert;
}

-(void)activeStoreKitAlert{
	
	NSString *message = [NSString stringWithFormat:@"Please wait for the existing transaction to complete"];
    
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    //[alert release];
    
}

-(void)canMakePaymentsAlert{
	
	
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"In-App Purchasing disabled" message:@"Check your parental control settings and try again later" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
    [alert show];
    //[alert release];
	
	
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (alertView.tag) {
        case ERROR_ALERT_TAG:[HTStoreManager setDelegate: nil];
            
            break;
            
        default:
            break;
    }
    
}

@end
