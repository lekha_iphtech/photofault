//  StoreManager.h
//  HTStoreKit
//  Created by Bruce Mackintosh on 16/08/2011.
//  Copyright 2011 Private. All rights reserved.


#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "HTStoreObserver.h"


#define SERVER_PRODUCT_MODEL 0
#define LICENSETYPE_KEY @"LincenceType1"



@protocol HTStoreKitDelegate <NSObject>

@optional

- (void)productFetchComplete:(NSMutableArray*)productArray;
- (void)productPurchased:(NSDictionary *)responseDictionary;
- (void)transactionCanceled:(NSString *)productId;
- (void)paymentQueueRestoreCompletedTransactionsFinished;
- (void)paymentQueueRestoreCompletedTransactionsFailed:(NSError*)error;
- (void)productFetchFail:(NSError *)error;

@end

@interface HTStoreManager : NSObject<SKProductsRequestDelegate, SKRequestDelegate , UIAlertViewDelegate> {

    NSMutableArray *_purchasableObjects;
	HTStoreObserver *_storeObserver;
	SKProductsRequest *productsRequest;
	UIAlertView * errorAlert;
	SKProduct *product;
	BOOL isStoreKitActive;
	BOOL isProductsAvailable;
	NSDictionary *verfiyDictionary;
}


@property (nonatomic, retain) NSMutableArray *purchasableObjects;
@property (nonatomic, retain) HTStoreObserver *storeObserver;
@property (nonatomic, retain) SKProductsRequest *productsRequest;
@property (nonatomic, retain) NSDictionary *verfiyDictionary;

+ (HTStoreManager*)sharedManager;

- (void) restorePreviousTransactions;
- (void) paymentQueueRestoreCompletedTransactionsFinished;
- (void) paymentQueueRestoreCompletedTransactionsFailed:(NSError *)error;
- (void) failedTransaction: (NSDictionary*) dictionary error:(NSError*)error;
- (void) requestProductData:(NSString *)productId;
//- (void)requestProductData;
- (BOOL) isStoreKitActive;
- (void) buyLicenseWithDictionary:(NSDictionary*)dict;
- (void) provideContent: (NSMutableDictionary*) dictionary;
- (void) transactionCanceled: (NSString *)clipId;
- (void)verifyLicenseWithDictionary:(NSDictionary*)dict;
- (UIAlertView *)errorAlert;
- (void)errorAlert:(NSError*)error;
-(void)buyLicenseProduct:(SKProduct *) productSelected;;
- (void) requestProductsData:(NSArray *)productIdArray ;
//DELEGATES
+(id)delegate;	
+(void)setDelegate:(id)newDelegate;

@end
