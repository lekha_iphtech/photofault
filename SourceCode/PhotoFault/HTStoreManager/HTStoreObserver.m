//
//  HTStoreObserver.m
//  HTStoreKit
//
//  Created by Bruce Mackintosh on 16/08/2011.
//  Copyright 2011 Private. All rights reserved.

#import "HTStoreObserver.h"
#import "HTStoreManager.h"



@interface HTStoreManager (InternalMethods)

- (void) transactionCanceled: (SKPaymentTransaction *)transaction;
- (void) failedTransaction: (SKPaymentTransaction *)transaction;
- (void) provideContent: (NSString*) productIdentifier forReceipt: (NSData*) recieptData;
- (NSString *) dateInFormat:(NSString*) stringFormat;
@end


@interface HTStoreObserver ()

- (NSString*) encode:(const uint8_t*) input length:(NSInteger) length;

@end


@implementation HTStoreObserver

@synthesize queue;
//@synthesize request;


#pragma mark -
#pragma mark MKStoreObserver


- (void) failedTransaction: (SKPaymentTransaction *)transaction{
    
	NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:[[HTStoreManager sharedManager] verfiyDictionary]];
	NSString *videoCode = [jsonDictionary objectForKey:@"videoCode"];
	
    
	[[HTStoreManager sharedManager] transactionCanceled:videoCode];
  
	if([transaction transactionState] != SKPaymentTransactionStatePurchasing){
		[[SKPaymentQueue defaultQueue] finishTransaction: transaction];	
	}
}

- (void) completeTransaction: (SKPaymentTransaction *)transaction{	
    
	[self verifyTransaction: transaction];
	if([transaction transactionState] != SKPaymentTransactionStatePurchasing){
		[[SKPaymentQueue defaultQueue] finishTransaction: transaction];	
 	}
}

- (void) restoreTransaction: (SKPaymentTransaction *)transaction{
	
	//[[MKStoreManager sharedManager] provideContent:transaction.payment.productIdentifier   forReceipt:transaction.transactionReceipt];
    [self verifyTransaction: transaction];
	if([transaction transactionState] != SKPaymentTransactionStatePurchasing){
		[[SKPaymentQueue defaultQueue] finishTransaction: transaction];	
	}
}


#pragma mark -
#pragma mark SKPaymentTransactionObserver

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    
    int counter = 0;
    
    
	for (SKPaymentTransaction *transaction in transactions)
	{
        
        counter++;
        
        
		switch (transaction.transactionState)
		{
			case SKPaymentTransactionStatePurchased:
            {
                NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
               [defs setBool:YES forKey:@"isPurchased"];
                [defs synchronize];
                
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Congratulations" message:@"Your transactions is completed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                //[alert release];
                
                
                
                NSLog(@"SKPaymentTransactionStatePurchased");
                [self completeTransaction:transaction];
            }
				
                break;
				
            case SKPaymentTransactionStateFailed:
            {
                
                
                NSLog(@"SKPaymentTransactionStateFailed");
                
                [self failedTransaction:transaction];
            }
                break;
				
            case SKPaymentTransactionStateRestored:
            {
				NSLog(@"SKPaymentTransactionStateRestored");
                
                
                
                
                [self restoreTransaction:transaction];

                
                
                
                
            }
				
            default:
				
				break;
		}
        
        
        
            
        
        
        
	}	
}










- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error{
	
	[[HTStoreManager sharedManager] paymentQueueRestoreCompletedTransactionsFailed:error];
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)myQueue{
    
    for (int i = 0; i<[[myQueue transactions] count]; i++) {
        SKPaymentTransaction *transaction = [[myQueue transactions] objectAtIndex:i];
        NSString *str = [[transaction payment] productIdentifier];
        NSLog(@"str %@",str);
        
    }
    
	[[HTStoreManager sharedManager] paymentQueueRestoreCompletedTransactionsFinished];
}

- (void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray *)transactions{
	NSLog(@"transactions = %@", transactions);
    
    SKPaymentTransaction *trans = [transactions objectAtIndex:0];
    NSLog(@"%@", trans.error);
	for (SKPaymentTransaction *aTransaction in transactions){
        
		if([aTransaction transactionState] != SKPaymentTransactionStatePurchasing){
			[[SKPaymentQueue defaultQueue] finishTransaction: aTransaction];	
		}
	}	
}

#pragma mark -
#pragma mark JSON Verification

- (NSString*) encode:(const uint8_t*) input length:(NSInteger) length {
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
	
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
	
    for (NSInteger i = 0; i < length; i += 3) {
        NSInteger value = 0;
        for (NSInteger j = i; j < (i + 3); j++) {
            value <<= 8;
			
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
		
        NSInteger index = (i / 3) * 4;
        output[index + 0] =                    table[(value >> 18) & 0x3F];
        output[index + 1] =                    table[(value >> 12) & 0x3F];
        output[index + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[index + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
	
    return [[NSString alloc] initWithData:data
                                 encoding:NSASCIIStringEncoding];
}

#pragma mark -
#pragma mark JSON verification using ASIHTTPRequest


-(NSOperationQueue*)queue{
	return queue;
}

- (void)verifyTransaction:(SKPaymentTransaction *)transaction {
    
    NSString *licenseKey = [[transaction payment] productIdentifier];
    
	if (![self queue]) {
		[self setQueue:[[NSOperationQueue alloc] init]];
	}
	NSString *receiptStr = nil;
	if(transaction){
		//NSData *dataReceipt = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
        //NSData *dataReceipt = [transaction transactionReceipt];   //cp 21 may
        
        NSData *dataReceipt = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
        receiptStr = [self encode:(uint8_t *)dataReceipt.bytes length:dataReceipt.length];    
	}
	
    
    NSMutableDictionary *jsonDictionary = [NSMutableDictionary dictionaryWithDictionary:[[HTStoreManager sharedManager] verfiyDictionary]];
   
    if (receiptStr) {
        
        if (licenseKey && ![licenseKey isEqualToString:@""]) {
            [jsonDictionary setObject:licenseKey forKey:LICENSETYPE_KEY];
        }
        
        [jsonDictionary setObject:receiptStr forKey:@"Receipt"];
        [[HTStoreManager sharedManager] provideContent:jsonDictionary];
        
    }
    else{
        
        NSError *error = [[NSError alloc] initWithDomain:@"Invalid Receipt" code:101 userInfo:nil];
		[[HTStoreManager sharedManager] failedTransaction:jsonDictionary error:error];
        
    }
	
	
	
}

-(NSString *) dateInFormat:(NSString*) stringFormat {
	char buffer[80];
	const char *format = [stringFormat UTF8String];
	time_t rawtime;
	struct tm * timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	strftime(buffer, 80, format, timeinfo);
	return [NSString  stringWithCString:buffer encoding:NSUTF8StringEncoding];
}
//
//#pragma mark -
//#pragma mark ASIHTTPRequestDelegate
//
//- (void)requestDone:(ASIHTTPRequest *)request{
//	
//	
//	//Status codes
//	/*
//	 200 = Everything correct 
//	 210 = Invalid receipt, but still send correct data response 
//	 400 = Malformed data 
//	 406 = Invalid clip ID with no data
//	 */
//	
//	int statusCode = [request responseStatusCode];
//    
//	NSDictionary *jsonDictionary = request.userInfo;
//	NSMutableDictionary *newDictionary = [NSMutableDictionary dictionaryWithDictionary:jsonDictionary];
//	
//	NSError *error = nil;
//	NSString *responseString = [request responseString];
//	NSDictionary *responseDict = nil;
//	if(statusCode == 200 || statusCode == 210){
//        responseDict = [responseString JSONValue];
//		if(responseDict){
//			NSString *purchaseUrl = [responseDict objectForKey:@"purchaseUrl"];
//			[newDictionary setValue:purchaseUrl forKey:@"purchaseUrl"];
//		}
//	}
//    
//	
//	if(statusCode == 200){
//		[[HTStoreManager sharedManager] provideContent:newDictionary];
//	}
//	else if(statusCode == 210){
//		[[HTStoreManager sharedManager] provideContent:newDictionary];
//	}
//	else if(statusCode == 400){
//		error = [[[NSError alloc] initWithDomain:@"Malformed Data:" code:statusCode userInfo:nil] autorelease];
//		[[HTStoreManager sharedManager] failedTransaction:jsonDictionary error:error];	
//	}
//	else if(statusCode == 406){
//		error = [[[NSError alloc] initWithDomain:@"Invalid Clip Id:" code:statusCode userInfo:nil] autorelease];
//		[[HTStoreManager sharedManager] failedTransaction:newDictionary error:error];	
//	}
//	else{
//		//500 error
//		error = [[[NSError alloc] initWithDomain:@"" code:statusCode userInfo:nil] autorelease];
//		[[HTStoreManager sharedManager] failedTransaction:newDictionary error:error];	
//	}	
//	
//#ifdef DEBUG
//	//[[HTStoreManager sharedManager] provideContent:jsonDictionary];
//#endif
//}
//
//- (void)requestWentWrong:(ASIHTTPRequest *)request{
//    
//	NSError *error = [request error];
//	NSDictionary *jsonDictionary = request.userInfo;
//	
//	[[HTStoreManager sharedManager] failedTransaction:jsonDictionary error:error];	
//}

#pragma mark -


@end
