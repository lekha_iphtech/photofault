//
//  CenterViewController.h
//  Navigation
//
//  Created by Tammy Coron on 1/19/13.
//  Copyright (c) 2013 Tammy L Coron. All rights reserved.
//

#import "LeftPanelViewController.h"
#import "SlideNavigationController.h"
#import "GPUImage.h"
#import <iAd/iAd.h>
#import "SettingsViewController.h"

/* GLOBAL VARIABLES */
UIImage *takenPhoto, *croppedImage;

@protocol CenterViewControllerDelegate <NSObject>

@optional
- (void)movePanelLeft;
- (void)movePanelRight;

@required
- (void)movePanelToOriginalPosition;

@end


@interface CenterViewController : UIViewController <LeftPanelViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, SlideNavigationControllerDelegate, ADBannerViewDelegate>
{
    
    GPUImageStillCamera *stillCamera;
    GPUImageOutput<GPUImageInput> *gpuFilter;
    //Nishant
   // GPUImageFilter *filter;
    GPUImageFilterGroup *grouoFilter;
    GPUImageView *filterView;
    
    BOOL frontCameraActive;
    BOOL flashon;
    
    NSInteger filterNr;
    UILabel *filterNameLabel;
    NSString *orientationStr;

    IBOutlet UIButton *flashon_offBtn;
    
    
    IBOutlet GPUImageView *mainView;
    
    
    
    SettingsViewController *settingVC;//Devesh june 19
    
}

@property (nonatomic, assign) id<CenterViewControllerDelegate> delegate;

@property (nonatomic, weak) IBOutlet UIButton *leftButton;
@property (nonatomic, weak) IBOutlet UIView *cropView;
@property (nonatomic, weak) IBOutlet UIImageView *backImgView;
@property (nonatomic, weak) IBOutlet GPUImageView *gpuImageView;
@property (nonatomic, weak) IBOutlet ADBannerView *banner;
@property (nonatomic, assign) BOOL bannerIsVisible;



- (IBAction)btnMovePanelRight:(id)sender;
- (IBAction)openGallary:(id)sender;
- (IBAction)clickPhoto:(id)sender;
- (IBAction)flashon_offClickedBtn:(id)sender;
- (IBAction)settingBtnClicked:(id)sender;
- (IBAction)nightModeBtnClicked:(id)sender;//nishant
- (IBAction)DefaultBtnClicked:(id)sender;//nishant
- (IBAction)ThermalBtnClicked:(id)sender;

-(void)menuItemDidSelected:(NSDictionary *)obj;


@end
