//
//  SettingCell.h
//  PhotoFault
//
//  Created by iPHTech1 on 19/06/15.
//  Copyright (c) 2015 Lekha Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *directionLbl;
//@property (retain, nonatomic) IBOutlet UILabel *photoFaultLbl;
@property (weak, nonatomic) IBOutlet UIImageView *photoPhaultImgView;

@end
