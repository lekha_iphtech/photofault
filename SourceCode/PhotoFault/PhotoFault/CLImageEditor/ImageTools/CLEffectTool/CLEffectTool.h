/*=====================
 -- PhotoFault --
 
 by iPHTech.
 =====================*/

#import "CLImageToolBase.h"

#import "CLEffectBase.h"
#import "Utility.h"

@interface CLEffectTool : CLImageToolBase
<CLEffectDelegate>

@end
