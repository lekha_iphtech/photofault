/*=====================
 -- PhotoGram --
 
 by AppsVilla Inc.
 =====================*/

#import <UIKit/UIKit.h>

@interface CLTextLabel : UILabel

@property (nonatomic, strong) UIColor *outlineColor;
@property (nonatomic, assign) CGFloat outlineWidth;

@end
