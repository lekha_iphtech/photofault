/*=====================
 -- PhotoFault --
 
 by iPHTech.
 =====================*/

#import <Foundation/Foundation.h>
#import "CLImageToolSettings.h"
#import "EditViewController.h"


static const CGFloat kCLImageToolAnimationDuration = 0.3;
static const CGFloat kCLImageToolFadeoutDuration   = 0.2;



@interface CLImageToolBase : NSObject <CLImageToolProtocol>

@property (nonatomic, weak) EditViewController *editor;//Lekha
@property (nonatomic, weak) CLImageToolInfo *toolInfo;

- (id)initWithImageEditor:(EditViewController*)editor withToolInfo:(CLImageToolInfo*)info;//Lekha

- (void)setup;
- (void)cleanup;
- (void)executeWithCompletionBlock:(void(^)(UIImage *image, NSError *error, NSDictionary *userInfo))completionBlock;

@end
