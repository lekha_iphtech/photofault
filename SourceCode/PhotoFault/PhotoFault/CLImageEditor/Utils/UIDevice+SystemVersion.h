/*=====================
 -- PhotoFault --
 
 by iPHTech.
 =====================*/

#import <UIKit/UIKit.h>

@interface UIDevice (SystemVersion)

+ (CGFloat)iosVersion;

@end
