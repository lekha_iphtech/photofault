//
//  LeftPanelViewController.m
//  SlideoutNavigation
//
//  Created by Tammy Coron on 1/10/13.
//  Copyright (c) 2013 Tammy L Coron. All rights reserved.
//

#import "LeftPanelViewController.h"
#import "Filter.h"
#import "AppDelegate.h"
#import "CenterViewController.h"
#import "EditViewController.h"

@interface LeftPanelViewController ()


@property (nonatomic, weak) IBOutlet UITableViewCell *cellMain;

@property (nonatomic, strong) NSMutableArray *arrayOfFilters;
@property (strong, nonatomic) NSMutableArray *filtersCategoryArray;

@end

@implementation LeftPanelViewController

#pragma mark -
#pragma mark View Did Load/Unload

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupFiltersArray];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

#pragma mark -
#pragma mark View Will/Did Appear

- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

#pragma mark -
#pragma mark View Will/Did Disappear

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

#pragma mark -
#pragma mark Array Setup


-(void)reloadData
{
    [self.myTableView reloadData];
    
    if ([[[[SlideNavigationController sharedInstance]  viewControllers] lastObject] isKindOfClass:[EditViewController class]]) {
        
        //EditViewController *editViewController = (EditViewController *)[[[SlideNavigationController sharedInstance]  viewControllers] lastObject];
        
      /*  if (takenPhoto) {
            
            [self.view setBackgroundColor:[UIColor colorWithPatternImage:takenPhoto]];
            
        }*/
    }

    
    
}
- (void)setupFiltersArray
{
    
    self.arrayOfFilters = [NSMutableArray array];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSMutableArray *categoryArray = [NSMutableArray array];
    self.filtersCategoryArray = [NSMutableArray array];
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"FiltersData" ofType:@"plist"];
    NSArray *fitersTempArray = [NSArray arrayWithContentsOfFile:plistPath];
    
    for (NSDictionary *infoDict in fitersTempArray) {
        
        NSString *categoryName = [infoDict objectForKey:@"cat_name"];
        
        
        if(![self.filtersCategoryArray containsObject:categoryName])
        {
            [categoryArray addObject:categoryName];
            [self.filtersCategoryArray addObject:categoryName];
            
        }
        
        Filter *filter = [[Filter alloc] init];
        
        filter.title = [infoDict objectForKey:@"title"];
        filter.num = [[infoDict objectForKey:@"num"] integerValue];
        filter.isCameraEffect = [[infoDict objectForKey:@"camera"] boolValue];
        
        [filter setCategoryName:categoryName];
        
        if(filter.isCameraEffect)
            [self.arrayOfFilters addObject:filter];
        
        [appDel.arrayOfFilters addObject:filter];
        
    }
    
    [appDel setFiltersCategoryArray:categoryArray];
    
    [self.myTableView reloadData];
    
}


-(BOOL)isHomeViewController
{
    
    if ([[[[SlideNavigationController sharedInstance]  viewControllers] lastObject] isKindOfClass:[CenterViewController class]]) {
        
        
        return YES;
    }
    
    return NO;
}
#pragma mark -
#pragma mark UITableView Datasource/Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self isHomeViewController]) {
        return [_arrayOfFilters count];
    }
    else
    {
        return [_filtersCategoryArray count];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellMainNibID = @"cellMain";
    
    _cellMain = [tableView dequeueReusableCellWithIdentifier:cellMainNibID];
    if (_cellMain == nil) {
        [[NSBundle mainBundle] loadNibNamed:@"MainCellLeft" owner:self options:nil];
    }
    
    UIImageView *mainImage = (UIImageView *)[_cellMain viewWithTag:1];
    
    UILabel *imageTitle = (UILabel *)[_cellMain viewWithTag:2];
    
    
    if ([self isHomeViewController]) {
        
    if ([_arrayOfFilters count] > 0)
    {
        Filter *filter = [self.arrayOfFilters objectAtIndex:indexPath.row];

        mainImage.image = [UIImage imageNamed:[filter.title lowercaseString]];
       
        imageTitle.text = [NSString stringWithFormat:@"%@", filter.title];
        
    }
    
    }
    else
    {
        if ([_filtersCategoryArray count] > 0)
        {
            NSString *cateName = [self.filtersCategoryArray objectAtIndex:indexPath.row];
            mainImage.image = [UIImage imageNamed:[cateName lowercaseString]];
            //mainImage.image = [UIImage imageNamed:@"kick"];
            imageTitle.text = @"";
            
        }
        
    }
    return _cellMain;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *userInfo;
    
    if ([self isHomeViewController]) {
        Filter *currentRecord = [self.arrayOfFilters objectAtIndex:indexPath.row];
        userInfo = [NSDictionary dictionaryWithObjectsAndKeys:currentRecord,@"filters",nil];
        
    }
    else
    {
        NSString *cateString = [self.filtersCategoryArray objectAtIndex:indexPath.row];
        NSArray *filtersArray = [self getFiltersForCategory:cateString];
        userInfo = [NSDictionary dictionaryWithObjectsAndKeys:filtersArray,@"filters", cateString, @"name",nil];

    }
    
    [[SlideNavigationController sharedInstance] menuItemsDidSelect:userInfo];
    
}

-(void)selectNextItem
{
    
    NSIndexPath *indexPath = [self.myTableView indexPathForSelectedRow];
    int rowNo = indexPath.row;
    rowNo = rowNo + 1;
    
    if (rowNo >= [_arrayOfFilters count]) {
        rowNo = 0;
    }
    
    indexPath = [NSIndexPath indexPathForItem:rowNo inSection:0];
    
    [self.myTableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:0];
    
    NSDictionary *userInfo;
    
    if ([self isHomeViewController]) {
        Filter *currentRecord = [self.arrayOfFilters objectAtIndex:indexPath.row];
        userInfo = [NSDictionary dictionaryWithObjectsAndKeys:currentRecord,@"filters",nil];
        [[SlideNavigationController sharedInstance] menuItemsDidSelect:userInfo];
    }
    
    
}

-(NSArray *)getFiltersForCategory:(NSString *)categoryName
{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.categoryName = %@", categoryName];
    
    AppDelegate *appDel = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray *fitersArray = [appDel.arrayOfFilters filteredArrayUsingPredicate:predicate];
    return fitersArray;
    
}

#pragma mark -
#pragma mark Default System Code

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
