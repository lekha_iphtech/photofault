
//  SlideoutNavigation
//
//  Created by Tammy Coron on 1/10/13.
//  Copyright (c) 2013 Tammy L Coron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Filter.h"

@implementation Filter



+ (GPUImageOutput<GPUImageInput> *)processFilterForNum:(int)no
{
    
    GPUImageOutput<GPUImageInput> *gpuFilter;
    
    switch (no) {
            
            //CHIC FILTERS SETTINGS
            
        case 0: // Eastern
            // Deafult Filter at app's startup
            gpuFilter = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageMonochromeFilter *)gpuFilter setColor:(GPUVector4)
             {237.0/255.0, 53.0/255.0, 53.0/255.0, 1.0}];
            [(GPUImageMonochromeFilter *)gpuFilter setIntensity: 0.4];
            break;
        case 1:  // Stella
            gpuFilter = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageMonochromeFilter *)gpuFilter setColor:(GPUVector4)
             {184.0/255.0, 44.0/255.0, 139.0/255.0, 1.0}];
            [(GPUImageMonochromeFilter *)gpuFilter setIntensity: 0.4];
            break;
        case 2: // Polaroid
            gpuFilter = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageMonochromeFilter *)gpuFilter setColor:(GPUVector4)
             {115.0/255.0, 184.0/255.0, 178.0/255.0, 1.0}];
            [(GPUImageMonochromeFilter *)gpuFilter setIntensity: 0.4];
            break;
        case 3:  // Breeze
            gpuFilter = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageMonochromeFilter *)gpuFilter setColor:(GPUVector4)
             {232.0/255.0, 218.0/255.0, 66.0/255.0, 1.0}];
            [(GPUImageMonochromeFilter *)gpuFilter setIntensity: 0.4];
            break;
        case 4:  // Saturn
            gpuFilter = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageMonochromeFilter *)gpuFilter setColor:(GPUVector4)
             {249.0/255.0, 148.0/255.0, 5.0/255.0, 1.0}];
            [(GPUImageMonochromeFilter *)gpuFilter setIntensity: 0.4];
            break;
        case 5:  // Yep
            gpuFilter = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageMonochromeFilter *)gpuFilter setColor:(GPUVector4)
             {18.0/255.0, 90.0/255.0, 210.0/255.0, 1.0}];
            [(GPUImageMonochromeFilter *)gpuFilter setIntensity: 0.4];
            break;
        case 6:  // NewTan
            gpuFilter = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageMonochromeFilter *)gpuFilter setColor:(GPUVector4)
             {180.0/255.0, 243.0/255.0, 112.0/255.0, 1.0}];
            [(GPUImageMonochromeFilter *)gpuFilter setIntensity: 0.4];
            break;
            
            
            
            
            // SILVER FILTERS SETTINGS
            
        case 7: { // Standard
            
            // Alloc and initialize a Filter Group instance
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            // Init a Monochrome filter
            GPUImageMonochromeFilter *mono = [[GPUImageMonochromeFilter alloc]init];
            // Add it to the Filter Group
            [(GPUImageFilterGroup *)gpuFilter addFilter: mono];
            
            // Init a Sturation Filter
            GPUImageSaturationFilter *saturation = [[GPUImageSaturationFilter alloc]init];
            // Add it to the Filter Group
            [(GPUImageFilterGroup *)gpuFilter addFilter: saturation];
            
            // Since you'll build an Array based on first Filter (mono), add the second filter as its target
            [mono addTarget:saturation];
            
            // Define initial and terminal Filters (in this case "mono" and "saturation")
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:mono]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:saturation];
            
            // Now you can set the values of each filter, acoordingly to their allowed settings, and you have to use the syntax below since those Filters are now into a Group:
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setColor:(GPUVector4) {1.0, 0.0, 0.0, 1.0}];
            
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setIntensity:0.3];
            
            [(GPUImageSaturationFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setSaturation:0.0];
            
            break; }
        case 8: { // B&W
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageMonochromeFilter *mono = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: mono];
            GPUImageSaturationFilter *saturation = [[GPUImageSaturationFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: saturation];
            
            [mono addTarget:saturation];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:mono]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:saturation];
            
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setColor:(GPUVector4) {0.0, 0.0, 1.0, 1.0}];
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setIntensity:0.7];
            [(GPUImageSaturationFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setSaturation:0.0];
            break; }
        case 9: { // Gray
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageMonochromeFilter *mono = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: mono];
            GPUImageSaturationFilter *saturation = [[GPUImageSaturationFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: saturation];
            
            [mono addTarget:saturation];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:mono]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:saturation];
            
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setColor:(GPUVector4) {1.0, 1.0, 1.0, 1.0}];
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setIntensity:0.8];
            [(GPUImageSaturationFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setSaturation:0.0];
            break; }
        case 10: { // Heavy
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageMonochromeFilter *mono = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: mono];
            GPUImageSaturationFilter *saturation = [[GPUImageSaturationFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: saturation];
            
            [mono addTarget:saturation];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:mono]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:saturation];
            
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setColor:(GPUVector4) {0.0, 0.0, 0.0, 0.0}];
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setIntensity:0.9];
            [(GPUImageSaturationFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setSaturation:0.0];
            break; }
        case 11: { // Faded
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageGrayscaleFilter *grayscale = [[GPUImageGrayscaleFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: grayscale];
            GPUImageContrastFilter *contrast = [[GPUImageContrastFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: contrast];
            
            [grayscale addTarget:contrast];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:grayscale]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:contrast];
            
            [(GPUImageContrastFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setContrast:0.5];
            break; }
        case 12: { // 80s
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageGrayscaleFilter *grayscale = [[GPUImageGrayscaleFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: grayscale];
            GPUImageContrastFilter *contrast = [[GPUImageContrastFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: contrast];
            GPUImageBrightnessFilter *brightness = [[GPUImageBrightnessFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: brightness];
            
            [grayscale addTarget:contrast];
            [grayscale addTarget:brightness];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:grayscale]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:brightness];
            
            [(GPUImageContrastFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setContrast:0.5];
            [(GPUImageBrightnessFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:2] setBrightness:0.5];
            break; }
        case 13: { // Noir
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageGrayscaleFilter *grayscale = [[GPUImageGrayscaleFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: grayscale];
            GPUImageContrastFilter *contrast = [[GPUImageContrastFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: contrast];
            GPUImageBrightnessFilter *brightness = [[GPUImageBrightnessFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: brightness];
            
            [grayscale addTarget:contrast];
            [grayscale addTarget:brightness];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:grayscale]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:brightness];
            
            [(GPUImageContrastFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setContrast:4.0];
            [(GPUImageBrightnessFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:2] setBrightness:-0.6];
            break; }
            
            
            
            
            
            //VINTAGE FILTERS SETTINGS
            
        case 14: { // Vinci
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageSepiaFilter *sepia = [[GPUImageSepiaFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: sepia];
            GPUImageExposureFilter *expos = [[GPUImageExposureFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: expos];
            
            [sepia addTarget:expos];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:sepia]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:expos];
            
            [(GPUImageSepiaFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setIntensity:0.8];
            [(GPUImageExposureFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setExposure: -1.0];
            break; }
        case 15: { // Edison
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageMonochromeFilter *mono = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: mono];
            GPUImageExposureFilter *expos = [[GPUImageExposureFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: expos];
            
            [mono addTarget:expos];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:mono]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:expos];
            
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setColor:(GPUVector4) {179.0/255.0, 97.0/255.0, 37.0/255.0,  0.5}];
            [(GPUImageExposureFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setExposure: -0.8];
            break; }
        case 16: { // Conrad
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageMonochromeFilter *mono = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: mono];
            GPUImageExposureFilter *expos = [[GPUImageExposureFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: expos];
            
            [mono addTarget:expos];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:mono]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:expos];
            
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setColor:(GPUVector4) {80.0/255.0, 110.0/255.0, 110.0/255.0,  0.5}];
            [(GPUImageExposureFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setExposure: -0.8];
            break; }
        case 17: { // Nobel
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageMonochromeFilter *mono = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: mono];
            GPUImageExposureFilter *expos = [[GPUImageExposureFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: expos];
            
            [mono addTarget:expos];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:mono]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:expos];
            
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setColor:(GPUVector4) {250.0/255.0, 245.0/255.0, 216.0/255.0,  0.5}];
            [(GPUImageExposureFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setExposure: -0.8];
            break; }
        case 18: { // Darwin
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageMonochromeFilter *mono = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: mono];
            GPUImageExposureFilter *expos = [[GPUImageExposureFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: expos];
            
            [mono addTarget:expos];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:mono]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:expos];
            
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setColor:(GPUVector4) {113.0/255.0, 11.0/255.0, 0.0/255.0,  0.6}];
            [(GPUImageExposureFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setExposure: -0.8];
            break; }
        case 19: { // Galileo
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageMonochromeFilter *mono = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: mono];
            GPUImageExposureFilter *expos = [[GPUImageExposureFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: expos];
            
            [mono addTarget:expos];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:mono]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:expos];
            
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setColor:(GPUVector4) {23.0/255.0, 77.0/255.0, 27.0/255.0,  0.6}];
            [(GPUImageExposureFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setExposure: -0.8];
            break; }
        case 20: { // Volta
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageMonochromeFilter *mono = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: mono];
            GPUImageExposureFilter *expos = [[GPUImageExposureFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: expos];
            
            [mono addTarget:expos];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:mono]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:expos];
            
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setColor:(GPUVector4) {255.0/255.0, 235.0/255.0, 119.0/255.0,  1.0}];
            [(GPUImageExposureFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setExposure: -0.4];
            break; }
            
            
            // DEEP FILTERS SETTINGS
        case 21: { // Golden
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageMonochromeFilter *mono = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: mono];
            GPUImageBrightnessFilter *brightness = [[GPUImageBrightnessFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: brightness];
            
            [mono addTarget:brightness];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:mono]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:brightness];
            
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setColor:(GPUVector4) {255.0/255.0, 235.0/255.0, 119.0/255.0,  1.0}];
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setIntensity:0.4];
            [(GPUImageBrightnessFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setBrightness:-0.4];
            break; }
        case 22: { // Green
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageMonochromeFilter *mono = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: mono];
            GPUImageBrightnessFilter *brightness = [[GPUImageBrightnessFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: brightness];
            
            [mono addTarget:brightness];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:mono]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:brightness];
            
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setColor:(GPUVector4) {176.0/255.0, 190.0/255.0, 89.0/255.0,  1.0}];
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setIntensity:0.4];
            [(GPUImageBrightnessFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setBrightness:-0.4];
            break; }
        case 23: { // Blue
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageMonochromeFilter *mono = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: mono];
            GPUImageBrightnessFilter *brightness = [[GPUImageBrightnessFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: brightness];
            
            [mono addTarget:brightness];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:mono]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:brightness];
            
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setColor:(GPUVector4) {28.0/255.0, 43.0/255.0, 97.0/255.0,  1.0}];
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setIntensity:0.4];
            [(GPUImageBrightnessFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setBrightness:-0.4];
            break; }
        case 24: { // Brown
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageMonochromeFilter *mono = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: mono];
            GPUImageBrightnessFilter *brightness = [[GPUImageBrightnessFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: brightness];
            
            [mono addTarget:brightness];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:mono]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:brightness];
            
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setColor:(GPUVector4) {98.0/255.0, 41.0/255.0, 14.0/255.0,  1.0}];
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setIntensity:0.4];
            [(GPUImageBrightnessFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setBrightness:-0.4];
            break; }
        case 25: { // Redish
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageMonochromeFilter *mono = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: mono];
            GPUImageBrightnessFilter *brightness = [[GPUImageBrightnessFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: brightness];
            
            [mono addTarget:brightness];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:mono]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:brightness];
            
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setColor:(GPUVector4) {194.0/255.0, 61.0/255.0, 0.0/255.0,  1.0}];
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setIntensity:0.4];
            [(GPUImageBrightnessFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setBrightness:-0.4];
            break; }
        case 26: { // Purple
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageMonochromeFilter *mono = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: mono];
            GPUImageBrightnessFilter *brightness = [[GPUImageBrightnessFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: brightness];
            
            [mono addTarget:brightness];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:mono]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:brightness];
            
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setColor:(GPUVector4) {124.0/255.0, 67.0/255.0, 94.0/255.0,  1.0}];
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setIntensity:0.4];
            [(GPUImageBrightnessFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setBrightness:-0.4];
            break; }
        case 27: { // Cyan
            gpuFilter = [[GPUImageFilterGroup alloc] init];
            
            GPUImageMonochromeFilter *mono = [[GPUImageMonochromeFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: mono];
            GPUImageBrightnessFilter *brightness = [[GPUImageBrightnessFilter alloc]init];
            [(GPUImageFilterGroup *)gpuFilter addFilter: brightness];
            
            [mono addTarget:brightness];
            [(GPUImageFilterGroup *)gpuFilter setInitialFilters:[NSArray arrayWithObject:mono]];
            [(GPUImageFilterGroup *)gpuFilter setTerminalFilter:brightness];
            
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setColor:(GPUVector4) {150.0/255.0, 213.0/255.0, 222.0/255.0,  1.0}];
            [(GPUImageMonochromeFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:0] setIntensity:0.4];
            [(GPUImageBrightnessFilter *)[(GPUImageFilterGroup *)gpuFilter filterAtIndex:1] setBrightness:-0.4];
            break; }
            
            
            // SPECIAL FILTERS SETTINGS
        case 28: { // Pxlt
            gpuFilter = [[GPUImagePixellateFilter alloc]init];
            [(GPUImagePixellateFilter *)gpuFilter setFractionalWidthOfAPixel:0.02];
            break; }
        case 29: { // Dotx
            gpuFilter = [[GPUImagePolkaDotFilter alloc]init];
            [(GPUImagePolkaDotFilter *)gpuFilter setFractionalWidthOfAPixel:0.02];
            break; }
        case 30: { // HalfTone
            gpuFilter = [[GPUImageHalftoneFilter alloc]init];
            [(GPUImageHalftoneFilter *)gpuFilter setFractionalWidthOfAPixel:0.025];
            break; }
        case 31: { // Sketch
            gpuFilter = [[GPUImageSketchFilter alloc]init];
            [(GPUImageSketchFilter *)gpuFilter setEdgeStrength:1.5];
            
            break; }
        case 32: { // Poster
            gpuFilter = [[GPUImagePosterizeFilter alloc]init];
            [(GPUImagePosterizeFilter *)gpuFilter setColorLevels:2.0];
            break; }
        case 33: { // Toony
            gpuFilter = [[GPUImageSmoothToonFilter alloc]init];
            [(GPUImageSmoothToonFilter *)gpuFilter setBlurRadiusInPixels:2.5];
            break; }
        case 34: { // Xoss
            gpuFilter = [[GPUImageCrosshatchFilter alloc]init];
            [(GPUImageCrosshatchFilter *)gpuFilter setCrossHatchSpacing:0.01];
            break; }
        default:
        {
            NSString *acvFileName = [NSString stringWithFormat:@"%d",no];
            gpuFilter = [[GPUImageToneCurveFilter alloc] initWithACV:acvFileName];
            
            
        break;
        }
    }
    
    return gpuFilter;
    
    
}

@end
