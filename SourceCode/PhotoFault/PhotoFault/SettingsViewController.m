//
//  SettingsViewController.m
//  PhotoFault
//
//  Created by Lekha Mishra on 20/05/15.
//  Copyright (c) 2015 Lekha Mishra. All rights reserved.
//

#import "SettingsViewController.h"
#import "Constants.h"
#import "EditViewController.h"

#import "Reachability.h"
#import "LoadingIndicator.h"

#define APP_ID 1011770410 //Todo
#define EMAIL_SUBJECT @"Hi there!"
#define EMAIL_MESSAGE @"Check Out the Awesome PhotoFault Photoeditng App #photofault - http://apple.co/1MfBa7W"
#define FACEBOOK_LINK @"https://www.facebook.com/photofault"
#define INSTAGRAM_LINK @"instagram://user?username=photofault"

#define kRemoveWatermarkProductIdentifier @"com.photofault.removewatermark"
#define kRemoveAdsProductIdentifier @"com.photofault.removeAds"
#define cameraEffectProductIdentifier @"com.photofault.specialcamera"
#define filtterSetProductIdentifier @"com.photofault.filterset1"

@interface SettingsViewController ()

@end

@implementation SettingsViewController
@synthesize  navigationView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    purchaseObjDic = [[NSMutableDictionary alloc]init];
    storeManager = [HTStoreManager sharedManager];
    [HTStoreManager setDelegate:self];
    
    // Array for Section Header titles.
    headerTitleAray = [[NSMutableArray alloc]  initWithObjects:@"WATERMARK",
                       @"SHOW SOME LOVE",
                       @"ABOUT",
                       nil];}

-(void)dismissPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - TABLEVIEW BUTTONS ==========================

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [headerTitleAray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section == 0)
        return 3;
    
    else if(section== 1)
        return 4;
    
    else
        return 2;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 49;
}


- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //Create a view for header.Height of header view return by header view method so we have to put same value here for header view as returned by method.
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 49)] ;
    [headerView setBackgroundColor:[UIColor colorWithRed:239.0f/255.0f green:239.0f/255.0f  blue:244.0f/255.0f  alpha:1 ]];
    
    //Both the upperlineView and bottomLine View are to show lines above and below the header view.
    UIView *upperLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 0.50f)] ;
    [upperLineView setBackgroundColor:[UIColor colorWithRed:155.0f/255.0f green:155.0f/255.0f  blue:155.0f/255.0f  alpha:1 ]];
    
    UIView *bottomlineView = [[UIView alloc] initWithFrame:CGRectMake(0, headerView.frame.size.height, tableView.bounds.size.width,0.50f)] ;
    [bottomlineView setBackgroundColor:[UIColor colorWithRed:155.0f/255.0f green:155.0f/255.0f  blue:155.0f/255.0f  alpha:1 ]];
    
    [headerView addSubview:upperLineView];
    [headerView addSubview:bottomlineView];
    
    //headerTitle is created to show name of section on header view.
    UILabel *headerTitle = [[UILabel alloc]initWithFrame:CGRectMake(15, 23, 170, 20)];
   // [ headerTitle setTextColor:[UIColor colorWithRed:130.0f/255.0f green:130.0f/255.0f  blue:135.0f/255.0f  alpha:1.0 ]];
    [ headerTitle setTextColor:[UIColor lightGrayColor]];
    [headerTitle setText:[headerTitleAray objectAtIndex :section ]];
    [headerTitle setFont:[UIFont fontWithName:@"FranklinGothic-Medium" size:15]];//Todo
    [headerView addSubview:headerTitle];
    
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 49;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"SettingCell";
    SettingCell *cell = (SettingCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
        
        cell = [nib objectAtIndex:0];
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    if (indexPath.section == 0)
    {
        switch (indexPath.row)
        {
            case 0:
                [cell.directionLbl setText:@"Remove Watermark"];
                break;
                
            case 1:
                [cell.directionLbl setText:@"No Ads"];
                break;
                
            case 2:
                [cell.directionLbl setText:@"Restore Purchase"];
                break;
                
            default:
                break;
        }
    }
    
    else if (indexPath.section == 1)
    {
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        
        switch (indexPath.row)
        {
            case 0:
                [cell.directionLbl setText:@"Rate this  app"];
                break;
                
            case 1:
                [cell.directionLbl setText:@"Tell a friend"];
                break;
                
            case 2:
                [cell.directionLbl setText:@"Photofault on Facebook"];
                break;
                
            case 3:
                [cell.directionLbl setText:@"Inspiration on Instagram"];
                break;
                
            default:
                break;
        }
    }
    
    else
    {
        if(indexPath.row==0)
        {
            [cell.directionLbl setText:@"Email the Developer"];
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        }
        if(indexPath.row==1)
            [cell.directionLbl setText:@"Version - 1.0"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(BOOL)checkInternetConnection
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if(internetStatus == NotReachable)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Note!" message:@"Check your Internet Connection." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [[LoadingIndicator currentIndicator]displayActivity:@"Loading..."];
        return YES;
    }
    return NO; 
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    NSInteger section = indexPath.section;
    
    switch (section) {
        case 0:
        {
            switch (row) {
                case 0: // remove watermark
                {
                    BOOL value = [[NSUserDefaults standardUserDefaults] boolForKey:WATERMARK_REMOVED];
                    
                    if (!value)
                    {
                        if([self checkInternetConnection])
                            [self buyProduct:kRemoveWatermarkProductIdentifier];
                    }
                    else
                    {
                        [[LoadingIndicator currentIndicator]displayCompleted:@"Done"];
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:WATERMARK_REMOVED];
                    }
                }
                    break;
                    
                case 1: // no ads
                {
                    BOOL value = [[NSUserDefaults standardUserDefaults] boolForKey:Ad_REMOVED];
                    
                    if (!value)
                    {
                        if([self checkInternetConnection])
                            [self buyProduct:kRemoveAdsProductIdentifier];
                    }
                    else
                    {
                        [[LoadingIndicator currentIndicator]displayCompleted:@"Done"];
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:Ad_REMOVED];
                    }
                }
                    break;
                    
                case 2: // restore purchase
                {
                    //Check for previous purchase at local end
                    
                    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
                    BOOL isAdPurchased = [userDef boolForKey:Ad_REMOVED];
                    BOOL isWatermarkPurchased = [userDef boolForKey:WATERMARK_REMOVED];
                    if(!isAdPurchased || !isWatermarkPurchased)
                    {
                        [storeManager restorePreviousTransactions];
                    }
                    else
                    {
                         [[LoadingIndicator currentIndicator]displayCompleted:@"Done"];
                        
                    }
                    
                }
                    break;
                    
                default:
                    break;
            }
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
            break;
            
        case 1:
        {
            switch (row) {
                case 0: // Rate App on the AppStore ========
                {
                    NSString *linkToReview = [NSString stringWithFormat:@"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%d", APP_ID];
                    
                    float ver_float = [[[UIDevice currentDevice] systemVersion] floatValue];
                    if(ver_float >= 7.0)
                    {
                        linkToReview = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%d", APP_ID];
                    }
                    
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:linkToReview]];
                }
                    break;
                    
                case 1:  // Tell a friend ==========
                {
                    // Alloc the Mail Composer Controller
                    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
                    mc.mailComposeDelegate = self;
                    [mc setSubject:EMAIL_SUBJECT];
                    //[mc setToRecipients:[NSArray arrayWithObjects:@"mishra.ni.lekha@gmail.com", nil]];
                    [mc setMessageBody:EMAIL_MESSAGE isHTML:true];
                    // Prepare the App Logo to be shared by Email
                    //UIImage *image = [UIImage imageNamed:@"appLogo"];
                   // NSData *imageData = UIImagePNGRepresentation(image);
                    //[mc addAttachmentData:imageData  mimeType:@"image/png" fileName:@"Logo.png"];
                    // Present Email View Controller
                    [self presentViewController:mc animated:true completion:NULL];
                }
                    break;
                    
                case 2: // PhotoFault on Facebook (link) ==============
                {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:FACEBOOK_LINK]];
                }
                    break;
                    
                case 3: // PhotoFault on Instagram (link) ==============
                {
                    NSURL *instagramURL = [NSURL URLWithString: INSTAGRAM_LINK];
                    if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
                    {
                        [[UIApplication sharedApplication] openURL:instagramURL];
                    }
                    else
                    {
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Instagram not found" message:@"Please install Instagram on your device!" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
                        [alert show];
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case 2:
        {
            if(row == 0)    // Email Developer ==============
            {
                MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
                mc.mailComposeDelegate = self;
                [mc setSubject:@"Support Request"];
                [mc setToRecipients:[NSArray arrayWithObjects:@"photofaults@gmail.com", nil]];
                [self presentViewController:mc animated:true completion:NULL];
            }
        }
            break;
            
        default:
            break;
    }
}



#pragma mark - EMAIL COMPOSER DELEGATE ========

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)results error:(NSError *)error {
    
    switch (results) {
        case MFMailComposeResultCancelled: {
            UIAlertView *myAlert = [[UIAlertView alloc]initWithTitle:@"Email Cancelled!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [myAlert show];
        } break;
            
        case MFMailComposeResultSaved:{
            UIAlertView *myAlert = [[UIAlertView alloc]initWithTitle:@"Email Saved!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [myAlert show];
        } break;
            
        case MFMailComposeResultSent:{
            UIAlertView *myAlert = [[UIAlertView alloc]initWithTitle:@"Email Sent!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [myAlert show];
        } break;
            
        case MFMailComposeResultFailed:{
            UIAlertView *myAlert = [[UIAlertView alloc]initWithTitle:@"Email error, try again!" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [myAlert show];
        } break;
            
            
        default: break;
    }
    // Dismiss the Email View Controller
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)backBtnclicked:(id)sender//nishant
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark-
#pragma mark IAP purchase method

-(void)buyProduct:(NSString *)productId
{
    SKProduct *product = [purchaseObjDic objectForKey:productId];
    
    if (product)
    {
        [storeManager buyLicenseProduct:product];
    }
    else
    {
        [storeManager requestProductData:productId];
    }
}


- (void)productFetchComplete:(NSMutableArray*)productArray
{
    for (SKProduct *product in productArray) {
        
        [purchaseObjDic setObject:product forKey:product.productIdentifier];
    }
    
    SKProduct *product = [purchaseObjDic objectForKey:kRemoveWatermarkProductIdentifier];
    
    if (product)
    {
        [storeManager buyLicenseProduct:product];
    }
    else
    {
        [self performSelectorOnMainThread:@selector(hideIndicator) withObject:nil waitUntilDone:YES];
    }
}


- (void)productFetchFail:(NSError *)error
{
    [self performSelectorOnMainThread:@selector(hideIndicator) withObject:nil waitUntilDone:YES];
    
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Note!" message:@"Unable to get product. Please try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

- (void)productPurchased:(NSDictionary *)responseDictionary
{
    // LincenceType1 = "com.photofault.removewatermark";
    NSString *productIdStr = [responseDictionary objectForKey:@"LincenceType1"];
    if([productIdStr isEqualToString:kRemoveWatermarkProductIdentifier])
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:WATERMARK_REMOVED];
    else if ([productIdStr isEqualToString:kRemoveAdsProductIdentifier])
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:Ad_REMOVED];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[LoadingIndicator currentIndicator]displayCompleted:@"Done"];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)transactionCanceled:(NSString *)productId
{
    [self performSelectorOnMainThread:@selector(hideIndicator) withObject:nil waitUntilDone:YES];
}

- (void)paymentQueueRestoreCompletedTransactionsFinished
{
    [self performSelectorOnMainThread:@selector(hideIndicator) withObject:nil waitUntilDone:YES];
}

- (void)paymentQueueRestoreCompletedTransactionsFailed:(NSError*)error
{
    [self performSelectorOnMainThread:@selector(hideIndicator) withObject:nil waitUntilDone:YES];
}


-(void)hideIndicator
{
    [[LoadingIndicator currentIndicator] hide];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end