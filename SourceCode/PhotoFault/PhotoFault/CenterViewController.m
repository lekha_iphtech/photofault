//
//  CenterViewController.m
//  Navigation
//
//  Created by Tammy Coron on 1/19/13.
//  Copyright (c) 2013 Tammy L Coron. All rights reserved.
//

#import "CenterViewController.h"
#import "SlideNavigationController.h"
#import "EditViewController.h"

#import "Filter.h"
#import "Constants.h"
#import "Utility.h"

@interface CenterViewController ()
{
    UIImage *croppedImage;
}

@property (nonatomic, strong) UIImage *photoSelected;

@property (nonatomic, strong) NSMutableArray *imagesArray;

@end

int whichMode = 2;
@implementation CenterViewController

#pragma mark -
#pragma mark View Did Load/Unload

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    [self setFrame];
    flashon = NO;
    [self setupFiltersAndCamera:nil];
    [self DefaultBtnClicked:self]; 
    
}


#pragma mark-  View Will/Did Appear

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    whichMode = 2;
    
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice");
    if (captureDeviceClass != nil)
    {
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if ([device hasTorch] && [device hasFlash])
        {
            [device lockForConfiguration:nil];
            if (device.flashMode == AVCaptureFlashModeOff)
            {
                [device setFlashMode:AVCaptureFlashModeOff];
                [flashon_offBtn setBackgroundImage:[UIImage imageNamed:@"flashoff"] forState:UIControlStateNormal];
                
            } else
            {
                [device setFlashMode:AVCaptureFlashModeOn];
                [flashon_offBtn setBackgroundImage:[UIImage imageNamed:@"flashon"] forState:UIControlStateNormal];
            }
            [device unlockForConfiguration];
        }
    }
    
    if(stillCamera)
        [stillCamera startCameraCapture];
    
    orientationStr = @"portrait";
    
}


- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

#pragma mark -
#pragma mark View Will/Did Disappear

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}


#pragma mark -
#pragma mark Default System Code

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark-
#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return NO;
}

#pragma Mark - Custom methods

-(void)setFrame
{
    int isIphone = [Utility isIphone];
    if(isIphone == iPhone5)
    {
    
        CGRect frm = mainView.frame;
        frm.origin.y = -10;
        mainView.frame = frm;
        
    }
}


#pragma mark -
#pragma mark Button Actions


- (IBAction)settingBtnClicked:(id)sender
{
    settingVC= [[SettingsViewController alloc]init];
    [self.navigationController pushViewController:settingVC animated:YES];
}


- (IBAction)flashon_offClickedBtn:(id)sender//nishant
{
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice");
    if (captureDeviceClass != nil) {
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if ([device hasTorch] && [device hasFlash]){
            
            [device lockForConfiguration:nil];
            if (device.flashMode == AVCaptureFlashModeOff)
            {
                [device setFlashMode:AVCaptureFlashModeOn];
                [sender setBackgroundImage:nil forState:UIControlStateNormal];
                [sender setImage:[UIImage imageNamed:@"flashon"]  forState:UIControlStateNormal];
            }
            else
            {
                [device setFlashMode:AVCaptureFlashModeOff];
                [sender setBackgroundImage:nil forState:UIControlStateNormal];
                [sender setImage:[UIImage imageNamed:@"flashoff"]  forState:UIControlStateNormal];
            }
            [device unlockForConfiguration];
        }
    }
}




- (IBAction)btnMovePanelRight:(id)sender
{
   [[SlideNavigationController sharedInstance] toggleLeftMenu];
    
}

- (IBAction)openGallary:(id)sender
{
    
    [stillCamera stopCameraCapture];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        [picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [self presentViewController:picker animated:YES completion:nil];
    }
}


- (IBAction)clickPhoto:(id)sender
{
    [self captureAndProcessPhoto];
}

- (IBAction)nightModeBtnClicked:(id)sender//nishant
{
    whichMode = 3;
    [self removeAllTargets];
    
    gpuFilter = [[GPUImageFilterGroup alloc]init];
    
    GPUImageFilter *customFilter = [[GPUImageFilter alloc] initWithFragmentShaderFromFile:@"eBlueBlack"];
    
    GPUImageHueFilter *hueFilter = [[GPUImageHueFilter alloc]init];
    [hueFilter setHue:100];
    
    [grouoFilter addTarget:customFilter];
    [customFilter addTarget:hueFilter];
    
    [(GPUImageFilterGroup *) gpuFilter setInitialFilters:[NSArray arrayWithObject: customFilter]];
    [(GPUImageFilterGroup *) gpuFilter setTerminalFilter:hueFilter];
    [stillCamera addTarget:gpuFilter];
    
    filterView = (GPUImageView *)mainView;
    
    [gpuFilter addTarget:filterView];
    [stillCamera startCameraCapture];
}

- (IBAction)DefaultBtnClicked:(id)sender
{
    whichMode = 2;
    [self removeAllTargets];
    gpuFilter = [[GPUImageFilter alloc] init];
    [stillCamera addTarget:gpuFilter];
    filterView = (GPUImageView *)mainView;
    
    [gpuFilter addTarget:filterView];
    [stillCamera startCameraCapture];
    
}

- (IBAction)ThermalBtnClicked:(id)sender
{
    whichMode = 1;
    [self removeAllTargets];
    gpuFilter = [[GPUImageToneCurveFilter alloc] initWithACV:@"thermal"];
    
    filterView = (GPUImageView *)mainView;
    [stillCamera addTarget:gpuFilter];
    [gpuFilter addTarget:filterView];
    [stillCamera startCameraCapture];
    
}


#pragma mark-
#pragma mark- Remove All Targets (thermal & Night Mode)

-(void) removeAllTargets
{
    [stillCamera removeAllTargets];
    [grouoFilter removeAllTargets];
}

#pragma mark - 
#pragma mark SWITCH CAMERA

- (IBAction)switchCamButt:(id)sender
{
    frontCameraActive = !frontCameraActive;
    [Utility playBeep];
    
    [stillCamera setHorizontallyMirrorFrontFacingCamera:true];
    [stillCamera rotateCamera];
}



#pragma mark-
#pragma mark- UIImagePickerDelegate methods-

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self dismissViewControllerAnimated:NO completion:nil];
    
    croppedImage = image;
    [self goToEditViewControllerDoCrop:NO];
    //[self openImageEditor];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:NO completion:nil];
    
    if(stillCamera)
        [stillCamera startCameraCapture];
    
}


#pragma mark -
#pragma mark - DEVICE ORIENTATION SETUP

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)orientationChanged:(NSNotification *)notification {
    UIDeviceOrientation newOrientation = [UIDevice currentDevice].orientation;
    
    if (newOrientation == UIDeviceOrientationLandscapeLeft) {
       // photoIsPortrait = false;
        orientationStr = @"landscapeLEFT";
        
        /*_timerLabel.transform =
        _switchCamOutlet.transform =
        _timerButtOutlet.transform =
        _randomOutlet.transform =
        _filtersButtOutlet.transform =
        CGAffineTransformMakeRotation(M_PI_2);*/
        
    }
    if (newOrientation == UIDeviceOrientationLandscapeRight) {
        //photoIsPortrait = false;
        orientationStr = @"landscapeRIGHT";
        
        /*_timerLabel.transform =
        _switchCamOutlet.transform =
        _timerButtOutlet.transform =
        _randomOutlet.transform =
        _filtersButtOutlet.transform =
        CGAffineTransformMakeRotation(-M_PI_2);*/
    }
    if (newOrientation == UIDeviceOrientationPortrait) {
        //photoIsPortrait = true;
        orientationStr = @"portrait";
        
        /*_timerLabel.transform =
        _switchCamOutlet.transform =
        _timerButtOutlet.transform =
        _randomOutlet.transform =
        _filtersButtOutlet.transform =
        CGAffineTransformMakeRotation(0);*/
    }
    
    NSLog(@"%@", orientationStr);
}




#pragma mark -
#pragma mark Delagate Method for capturing selected image


- (void)filterSelected:(Filter *)filtr
{
    // only change the main display if an animal/image was selected
    if (filtr)
    {
        [Utility playBeep];
        [self showFilterLabel:filtr.title];
        
        [stillCamera stopCameraCapture];
        [self setupFiltersAndCamera:filtr];
        
    }
}
-(void)menuItemDidSelected:(NSDictionary *)obj
{
    
    NSLog(@"CenterViewController %@", obj);
    
    Filter *filtr = [obj objectForKey:FILTERS];
    [self filterSelected:filtr];
    
}

-(void)showFilterLabel:(NSString *)filterName {
    // Show a Label with the filter's name
    filterNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 100)];
    filterNameLabel.center = CGPointMake(_gpuImageView.frame.size.width/2, _gpuImageView.frame.size.height/2);
    //filterNameLabel.font = FILTER_NAME_FONT;//Lekha
    filterNameLabel.textColor = [UIColor whiteColor];
    filterNameLabel.textAlignment = NSTextAlignmentCenter;
    filterNameLabel.text = filterName;
    filterNameLabel.tag = 2000;
    [self.view addSubview:filterNameLabel];
    
    // Rotate this label accoordingly to the device orientation
    if ([orientationStr isEqualToString:@"landscapeRIGHT"]) {
        filterNameLabel.transform =
        CGAffineTransformMakeRotation(-M_PI_2);
    } else if ([orientationStr isEqualToString:@"landscapeLEFT"]) {
        filterNameLabel.transform =
        CGAffineTransformMakeRotation(M_PI_2);
    } else if ([orientationStr isEqualToString:@"portrait"]){
        filterNameLabel.transform =
        CGAffineTransformMakeRotation(0);
    }
    
    [self performSelector:@selector(removeLabel) withObject:self];
    
    // Save the last Filter used & its name
    [[NSUserDefaults standardUserDefaults] setInteger: filterNr forKey:@"filterNr"];
    //[[NSUserDefaults standardUserDefaults] setObject:filterNameStr forKey: @"filterNameStr"];
    
}

// Remove that Label that shows a filter name over the camera screen
-(void)removeLabel {
    [UIView animateWithDuration:0.3 delay:0.8 options:UIViewAnimationOptionCurveLinear animations:^ {
        filterNameLabel.alpha = 0;
    } completion:^(BOOL finished) {
        [[self.view viewWithTag:2000]removeFromSuperview];
    }];
}


- (void)setupFiltersAndCamera:(Filter *)filtr
{
    
    int isIphone = [Utility isIphone];
    
    AVCaptureDevicePosition avCaptureDevicePosition = AVCaptureDevicePositionBack;
    if (frontCameraActive) {
        avCaptureDevicePosition = AVCaptureDevicePositionFront;
    }
    
    NSString *capturePreset = AVCaptureSessionPresetHigh;
    if (isIphone == iPhone4) {
        capturePreset = AVCaptureSessionPresetHigh;
    }
    
    if(stillCamera)
    {
        stillCamera = nil;
    }
    
     stillCamera = [[GPUImageStillCamera alloc] initWithSessionPreset:capturePreset cameraPosition:avCaptureDevicePosition];
   
     stillCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
    
    if (frontCameraActive) {
         [stillCamera setHorizontallyMirrorFrontFacingCamera:true];
    }
    
    if (filtr) {
        gpuFilter = [Filter processFilterForNum:(int)filtr.num];
        GPUImageView *filterVw = (GPUImageView *)mainView;
        [gpuFilter addTarget:filterVw];
        [stillCamera addTarget:gpuFilter];
    }
    
    [stillCamera startCameraCapture];
    
}

// Capture & process the photo after tapping snapPic Button

-(void)captureAndProcessPhoto {
    
    [Utility playCamClick];

    if (whichMode == 1)
    {
        whichMode = 0;
        
    }
    else if (whichMode == 2)
    {
        whichMode = 0;
        
    }
    else if (whichMode == 3)
    {
        whichMode = 0;
        
    }
    else if (whichMode == 0)
    {

    }

    //  NSLog(@" stillCamera.framebufferForOutput.size %f %f",  stillCamera.framebufferForOutput.size.width,  stillCamera.framebufferForOutput.size.height);
    
    /*[stillCamera capturePhotoAsJPEGProcessedUpToFilter:gpuFilter withCompletionHandler:^(NSData *processedJPEG, NSError *error){
        
        UIImage *processedImage = [UIImage imageWithData:processedJPEG];
        [self goToEditViewController:processedImage doCrop:YES];
        
    }];*/
    
     [stillCamera capturePhotoAsImageProcessedUpToFilter:gpuFilter withOrientation:UIImageOrientationUp withCompletionHandler:^(UIImage *processedImage, NSError *error) {
      
        
        // Process photo
        //takenPhoto = processedImage;
        //_camImageView.image = processedImage;
        //_snapPicOutlet.enabled = true;
        // takenPhoto = processedImage;
        [self goToEditViewControllerDoCrop:YES];
        
    }];
    
    
}

-(void)cropImage {
    CGRect rect = _cropView.bounds;
    UIGraphicsBeginImageContextWithOptions(rect.size, true, 0.0);
    [_cropView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *resultImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    croppedImage = resultImg;
 
}

/*
-(UIImage*)cropImage:(UIImage*)img
{
    // 64 for topheader , 85 for bottom header
    //float factor = img.size.height/self.view.frame.size.height;
    
    float factor = img.size.width/self.view.frame.size.width;
    
    CGSize _size = CGSizeMake(img.size.width, img.size.height-((64+85)*factor));
    float scale = [UIScreen mainScreen].scale;
    UIGraphicsBeginImageContextWithOptions(_size, NO, scale);
    [img drawInRect:CGRectMake(0, -64*factor, img.size.width, img.size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
    
}
*/

-(void)goToEditViewControllerDoCrop:(BOOL)crop
{
    if(crop)
    {
        [self cropImage];
    }
    
    [stillCamera stopCameraCapture];
    EditViewController *editViewController = [[EditViewController alloc] initWithNibName:@"EditViewController" bundle:nil];
    
    NSData *imgData = UIImageJPEGRepresentation(croppedImage, 1.0);
    [editViewController setRawImage:[UIImage imageWithData:imgData]];
    
    [[SlideNavigationController sharedInstance] pushViewController:editViewController animated:YES];
}

#pragma mark-
#pragma mark For Detecting Mothions-

- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if(event.type == UIEventSubtypeMotionShake)
    {
        [[SlideNavigationController sharedInstance] selectNextItem];
        
    }
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}


#pragma mark-
#pragma mark BannerViews Methods-


- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    [self.banner setHidden:NO];
}


- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    [self.banner setHidden:YES];
}


@end
