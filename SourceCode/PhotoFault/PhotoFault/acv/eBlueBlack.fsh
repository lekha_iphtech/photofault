#ifdef GL_ES
// define default precision for float, vec, mat.
precision highp float;
#endif

uniform sampler2D   inputImageTexture;
//uniform vec3       minrgb;
//uniform vec3       maxrgb;

varying vec2 textureCoordinate;


void main()
{
    vec4        colora = texture2D(inputImageTexture, textureCoordinate);
    vec3        minrgb = vec3( 0.05 , 0.31 , 0.89);
    vec3        maxrgb = vec3(0.44 , 0.665, 0.91);    // delete these 2 lines and use and make these uniform
    // step 1. gray 
    //
    
    float   avg = (colora.r +colora.g + colora.b)/ 3.0;
    vec3    gray = clamp(vec3(avg), 0.0, 1.0);
    
    
    // step 2. tint
    //
	gl_FragColor = vec4(clamp(vec3(gray - minrgb) * (1.0 / (maxrgb - minrgb)), 0.0, 1.0), 1.0);
}
