//
//  Utility.h
//  PhotoFault
//
//  Created by Lekha Mishra on 28/05/15.
//  Copyright (c) 2015 Lekha Mishra. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define iPhone4 1
#define iPhone5 2
#define iPhone6 3
#define iPhone6Plus 4

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

@interface Utility : NSObject

+(void)playCamClick;
+(void)playBeep;
+ (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size;
+(CGRect) getUpdatedFrame :(UIImage *)image with :(UIImageView *)imageView withActualFrame:(CGRect) actualImageFrame;
+(int)isIphone;
@end
