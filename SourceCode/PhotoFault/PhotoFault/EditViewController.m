//
//  EditViewController.m
//  PhotoFault
//
//  Created by Lekha Mishra on 20/05/15.
//  Copyright (c) 2015 Lekha Mishra. All rights reserved.
//

#define SHARE_CONTANT @"Check Out the Awesome PhotoFault Photoeditng App #photofault - http://apple.co/1MfBa7W"

#import "EditViewController.h"
#import "SlideNavigationController.h"
#import "BlurredView.h"
#import "Filter.h"
#import "Constants.h"
#import "Utility.h"
#import "CLImageToolInfo.h" //Lekha 25June
#import "CLImageToolBase.h"//Lekha
#import "SettingsViewController.h"
#import "CenterViewController.h"

@interface EditViewController ()<CLImageToolProtocol>

@property (nonatomic, strong, readwrite) CLImageToolInfo *toolInfo; //Lekha 25June
@property (nonatomic, strong, readwrite) NSMutableArray *toolsArray; //Lekha 25June
@property (nonatomic, strong) CLImageToolBase *currentTool;//Lekha 25June

@end

int checkAdMode = 0;//nishant

@implementation EditViewController

@synthesize adBannerView;
@synthesize watermarkImageView;
@synthesize originalImageSize;//Vish

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _imageView.image = self.rawImage;
    
    //takenPhoto = [BlurredView blur:croppedImage];
    
    //backImgView.image = takenPhoto;
    
    self.toolsArray = [[NSMutableArray alloc] init];//Lekha
    [self.toolsArray addObjectsFromArray:[self setUpTools]];//Lekha
    [self sortedSubtools];//Lekha
    
    
   
    
    //NSLog(@" Tools Array %@", self.toolsArray);
    count = 4;
    
  
    
}

-(void)viewWillAppear:(BOOL)animated
{
    BOOL prefs = [[NSUserDefaults standardUserDefaults] boolForKey:WATERMARK_REMOVED];
    
    if(prefs)
    {
        [watermarkImageView setHidden:YES];
    }
    else
    {
         [self setWaterMarkFrame];
        [watermarkImageView setHidden:NO];
        
    }
   
    [self updateBottomScrollView];
    
    [super viewWillAppear:animated];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//Lekha Start======

-(void)updateBottomScrollView
{
    
    CGSize size = [self.bottomMenuScrollView contentSize];
    
    int noOfItems = 8;
    int itemWithd = 60;
    int middleOffSet = 5;
    
    if (IS_IPAD) {
        
        middleOffSet = 30;
        
        
    }
    
    
    for (int i = 0; i<[[self.bottomMenuScrollView subviews] count]; i++) {
        
        id obj = [[self.bottomMenuScrollView subviews] objectAtIndex:i];
        
        if ([obj isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton *)obj;
            CGRect frame = btn.frame;
            NSInteger tagVlaue = [btn tag];
            
            frame.origin.x = (tagVlaue - 2)*itemWithd + (tagVlaue - 1)*middleOffSet;
            [btn setFrame:frame];
            
            
        }
    }
    
    
    size.width = noOfItems*(itemWithd + middleOffSet)+middleOffSet;
    
    [self.bottomMenuScrollView setContentSize:size];
    
}

#pragma mark-
#pragma mark - CLImageToolProtocol methods-

+ (NSString*)defaultIconImagePath
{
    
    return nil;
    
}


+ (CGFloat)defaultDockedNumber
{
    return 0;
    
}

+ (NSString*)defaultTitle
{
     return NSLocalizedStringWithDefaultValue(@"CLImageEditor_DefaultTitle", nil, [CLImageEditorTheme bundle], @"Edit", @"");
    
}

+ (BOOL)isAvailable
{
    
     return YES;
    
}

+ (NSArray*)subtools
{
    
    return [CLImageToolInfo toolsWithToolClass:[CLImageToolBase class]];
    
}

+ (NSDictionary*)optionalInfo
{
    
    return nil;
    
    
}

-(NSArray *)setUpTools
{
    return [CLImageToolInfo toolsWithToolClass:[CLImageToolBase class]];
    
}



- (void)setCurrentTool:(CLImageToolBase *)currentTool
{
    if(currentTool != _currentTool){
        [_currentTool cleanup];
        _currentTool = currentTool;
        [_currentTool setup];
        
        [self swapToolBarWithEditting:(_currentTool!=nil)];
    }
}

- (void)sortedSubtools
{
        NSArray *tempToolsArray = [self.toolsArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        CGFloat dockedNum1 = [obj1 dockedNumber];
        CGFloat dockedNum2 = [obj2 dockedNumber];
        
        if(dockedNum1 < dockedNum2){ return NSOrderedAscending; }
        else if(dockedNum1 > dockedNum2){ return NSOrderedDescending; }
        return NSOrderedSame;
    }];
    
    
    [self.toolsArray removeAllObjects];
    [self.toolsArray addObjectsFromArray:tempToolsArray];
    

}


- (void)swapNavigationBarWithEditting:(BOOL)editting
{
    /*if(self.navigationController==nil){
        return;
    }
    
    [self.navigationController setNavigationBarHidden:editting animated:YES];
    
    if(editting){
        _navigationBar.hidden = NO;
        _navigationBar.transform = CGAffineTransformMakeTranslation(0, -_navigationBar.height);
        
        [UIView animateWithDuration:kCLImageToolAnimationDuration
                         animations:^{
                             _navigationBar.transform = CGAffineTransformIdentity;
                         }
         ];
    }
    else{
        [UIView animateWithDuration:kCLImageToolAnimationDuration
                         animations:^{
                             _navigationBar.transform = CGAffineTransformMakeTranslation(0, -_navigationBar.height);
                         }
                         completion:^(BOOL finished) {
                             _navigationBar.hidden = YES;
                             _navigationBar.transform = CGAffineTransformIdentity;
                         }
         ];
    }*/
}


- (void)swapToolBarWithEditting:(BOOL)editting
{
    /*[self swapMenuViewWithEditting:editting];
    [self swapNavigationBarWithEditting:editting];
    
    if(self.currentTool){
        UINavigationItem *item  = [[UINavigationItem alloc] initWithTitle:self.currentTool.toolInfo.title];
        item.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringWithDefaultValue(@"CLImageEditor_OKBtnTitle", nil, [CLImageEditorTheme bundle], @"OK", @"") style:UIBarButtonItemStyleDone target:self action:@selector(pushedDoneBtn:)];
        
        item.leftBarButtonItem  = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringWithDefaultValue(@"CLImageEditor_BackBtnTitle", nil, [CLImageEditorTheme bundle], @"Back", @"") style:UIBarButtonItemStylePlain target:self action:@selector(pushedCancelBtn:)];
        
        [_navigationBar pushNavigationItem:item animated:(self.navigationController==nil)];
    }
    else{
        [_navigationBar popNavigationItemAnimated:(self.navigationController==nil)];
    }*/
}


- (void)setupToolWithToolInfo:(CLImageToolInfo*)info
{
    
    [self openSubView];
    
    if(self.currentTool){ return; }
    
    Class toolClass = NSClassFromString(info.toolName);
    
    if(toolClass){
        id instance = [toolClass alloc];
        if(instance!=nil && [instance isKindOfClass:[CLImageToolBase class]]){
            self.originalImageSize = self.imageView.image.size;//Vish
            instance = [instance initWithImageEditor:self withToolInfo:info];
            self.currentTool = instance;
        }
    }
}


//Lekha End======

#pragma mark-
#pragma mark- MenuSelected Methods-

-(void)menuItemDidSelected:(NSDictionary *)obj
{
    [slideFilterPanelBtn setTitle:[obj objectForKey:@"name"] forState:UIControlStateNormal];
    [slideFilterPanelBtn setHidden:NO];
    id filter = [obj objectForKey:FILTERS];
    
    if ([filter isKindOfClass:[NSArray class]]) {
         [self filterSelected:filter];
    }
}


#pragma mark- Filters Methods-

- (void)filterSelected:(NSArray *)filter
{
    // only change the main display if an animal/image was selected
    if (filter)
    {
        //[self showFilterLabel:filter.title];
        
       // [self setupFilters:filter];
        self.filtersArray = filter;
        
        [Utility playBeep];
        [self closeTool];
        [self setupToolWithToolInfo:[self.toolsArray  objectAtIndex:kFilterType]];
    }
}


- (void)setupFilters:(Filter *)filter
{
   /* [stillCamera addTarget:gpuFilter];
    GPUImageView *filterView = (GPUImageView *)self.view;
    [gpuFilter addTarget:filterView];
    
    [stillCamera startCameraCapture];
    */
    
    
   /* GPUImagePicture *stillImageSource = [[GPUImagePicture alloc] initWithImage:self.rawImage];

    GPUImageOutput<GPUImageInput> *gpuFilter = [filter processFilter];
    [stillImageSource addTarget:gpuFilter];
    [stillImageSource useNextFrameForImageCapture];
    [stillImageSource processImage];
    
    UIImage *currentFilteredVideoFrame = [gpuFilter imageFromCurrentFramebuffer];
    
    imgView.image = currentFilteredVideoFrame;*/
    
    /*GPUImageOutput<GPUImageInput> *gpuFilter = [filter processFilter];
    UIImage *currentFilteredVideoFrame = [gpuFilter imageByFilteringImage:self.rawImage];
    self.imageView.image = currentFilteredVideoFrame;*/
    
}

#pragma mark-
#pragma mark Image Editing finish methods-

- (void)imageEditorDidFinishEdittingWithImage:(UIImage *)image
{
    _imageView.image = image;
    [self refreshImageView];
    
}

- (void)imageEditorWillDismissWithImageView:(UIImageView *)imageView canceled:(BOOL)canceled
{
    [self refreshImageView];
}



-(void)closeTool
{
    if (_currentTool) {
        [_currentTool cleanup];
        _currentTool = nil;
    }
}


#pragma mark- IBActions Methods-

-(IBAction)filtersBtnClicked:(id)sender
{
    [self calculateInterstitialAdsCounts];
    [[SlideNavigationController sharedInstance] toggleLeftMenu];

}

-(IBAction)slideFilterBtnClicked:(id)sender
{
   // [slideFilterPanelBtn setHidden:YES];
    [[SlideNavigationController sharedInstance] toggleLeftMenu];
}


-(IBAction)fxBtnClicked:(id)sender
{
    
    [self calculateInterstitialAdsCounts];
   
    [self closeTool];
    
    [self setupToolWithToolInfo:[self.toolsArray  objectAtIndex:kEffectType]];
   
    //[self setupToolWithToolInfo:[self.toolsArray  objectAtIndex:kFrameType]];
    
}

-(IBAction)ShareBtnClicked:(id)sender
{
    
     //[self setupToolWithToolInfo:[self.toolsArray  objectAtIndex:kResizeType]];
    
    [self calculateInterstitialAdsCounts];
    
    NSURL *fileURL;
    docInteracCont.delegate = self;
    
    //Save the Image to default Document directory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:TEMP_PIC_NAME];
    
    self.rawImage = _imageView.image;
    
    BOOL prefs = [[NSUserDefaults standardUserDefaults ]boolForKey:WATERMARK_REMOVED];
    if(!prefs)
    {
        UIImage *img = [UIImage imageNamed:WATERMARK_IMAGE];
        //self.img.contentMode = UIViewContentModeScaleAspectFill;
        
        //CGSize imageSize = self.imageView.image.size;
        //CGSize scaledImageSize = CGSizeMake(imageSize.width, imageSize.height);
        CGRect newframe = CGRectMake(roundf(self.rawImage.size.width)-130, roundf(self.rawImage.size.height)-50, 111, 22);
        
        self.rawImage = [self layerViewFirstLayer:self.rawImage SecongLayer:img secondFrame:newframe];
    }

    
    NSData *imageData = UIImageJPEGRepresentation(self.rawImage, 1.0);
    
    [imageData writeToFile:savedImagePath atomically:false];
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:TEMP_PIC_NAME];
    fileURL = [[NSURL alloc] initFileURLWithPath:getImagePath];
    
//    // Open the Document Interaction controller for Sharing options (in Menu)
//    docInteracCont = [UIDocumentInteractionController interactionControllerWithURL:fileURL];
//    [docInteracCont presentOpenInMenuFromRect:CGRectZero inView:self.view animated:true];
    
    NSString *textToShare = SHARE_CONTANT;
    
    NSArray *objectsToShare = @[textToShare, fileURL];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    activityVC.excludedActivityTypes = @[UIActivityTypeCopyToPasteboard];
    activityVC.popoverPresentationController.sourceView = self.view;
    
    [self presentViewController:activityVC animated:YES completion:nil];
    
}

-(IBAction)cropBtnClicked:(id)sender
{
    
    [self calculateInterstitialAdsCounts];
    [self closeTool];
    [self setupToolWithToolInfo:[self.toolsArray objectAtIndex:kCropType]];
    //[self setupToolWithToolInfo:[self.toolsArray  objectAtIndex:kStickerType]];
}

-(IBAction)rotateButtonClicked:(id)sender
{
    [watermarkImageView setHidden:YES];
    [self calculateInterstitialAdsCounts];
    [self closeTool];
    [self setupToolWithToolInfo:[self.toolsArray objectAtIndex:kRotateType]];
}

-(IBAction)toneCurveButtonClicked:(id)sender
{
    [self calculateInterstitialAdsCounts];
    [self closeTool];
    [self setupToolWithToolInfo:[self.toolsArray objectAtIndex:kToneType]];
}

-(IBAction)blurAndFocusButtonClicked:(id)sender
{
    [self calculateInterstitialAdsCounts];
    [self closeTool];
    [self setupToolWithToolInfo:[self.toolsArray objectAtIndex:kBlurType]];
}

-(IBAction)cancelButtonClicked:(id)sender
{
    [self pushedCancelBtn:nil];
    [self closeSubView];
}

-(IBAction)doneButtonClicked:(id)sender
{
    [watermarkImageView setHidden:NO];
    [self pushedDoneBtn:nil];
    [self setWaterMarkFrame];
    [self closeSubView];
}

-(void)closeSubView
{
    [slideFilterPanelBtn setHidden:YES];
    [self.menuView setHidden:YES];
    [self.topConfirmationBar setHidden:YES];
    [self.topBar setHidden:NO];
    
    if (_currentTool) {
        [_currentTool cleanup];
        _currentTool = nil;
    }
    
}

-(void)openSubView
{
    [self.menuView setHidden:NO];
    [self.topConfirmationBar setHidden:NO];
    [self.topBar setHidden:YES];
}

-(void)calculateInterstitialAdsCounts
{
    checkAdMode = checkAdMode + 1;
    if (checkAdMode >= 5)
    {
        [self createScreenwideAd];
        checkAdMode = 0;
    }
}

-(IBAction)backButtonClickedLibrary:(id)sender
{
    
     [[SlideNavigationController sharedInstance] popViewControllerAnimated:NO];
    
}

-(IBAction)saveToPhotoLibrary:(id)sender
{

    [self calculateInterstitialAdsCounts];
    
    outPutImage = self.imageView.image;
    
    
    BOOL prefs = [[NSUserDefaults standardUserDefaults ]boolForKey:WATERMARK_REMOVED];
    if(!prefs)
    {
        UIImage *img = [UIImage imageNamed:WATERMARK_IMAGE];
        //self.img.contentMode = UIViewContentModeScaleAspectFill;
        
        //CGSize imageSize = self.imageView.image.size;
        //CGSize scaledImageSize = CGSizeMake(imageSize.width, imageSize.height);
        CGRect newframe = CGRectMake(roundf(outPutImage.size.width)-130, roundf(outPutImage.size.height)-50, 111, 22);
        
        outPutImage = [self layerViewFirstLayer:outPutImage SecongLayer:img secondFrame:newframe];
    }
//
//    
//    
//    
//    UIImageWriteToSavedPhotosAlbum(outPutImage, nil, nil, nil);
//    
//    UIAlertView *myAlert = [[UIAlertView alloc]initWithTitle:@"PhotoFault"
//                                                     message:@"Image saved to PhotoLibrary!"
//                                                    delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];//Lekha
//    [myAlert show];
    
    
    //Share on Instagram

    


    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        // Set DocumentInteractionController delegate
        docInteracCont.delegate = self;
        
        //Saves the edited Image to directory
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"Retrika.jpg"];
        
        NSData *imageData = UIImagePNGRepresentation(outPutImage);
        [imageData writeToFile:savedImagePath atomically:false];
        
        //Load the edited Image
        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"Retrika.jpg"];
        UIImage *tempImage = [UIImage imageWithContentsOfFile:getImagePath];
        
        //Hooks the edited Image with Instagram
        NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Image.igo"];
        [UIImageJPEGRepresentation(tempImage, 1.0) writeToFile:jpgPath atomically:YES];
        
        // Prepares the DocumentInteraction with the .igo image for Instagram
        NSURL *instagramImageURL = [[NSURL alloc] initFileURLWithPath:jpgPath];
        docInteracCont = [UIDocumentInteractionController interactionControllerWithURL:instagramImageURL];
        [docInteracCont setUTI:@"com.instagram.exclusivegram"];
         CGRect rect = CGRectMake(0 ,0 , 0, 0);
        // Opens the DocumentInteraction Menu
        [docInteracCont presentOpenInMenuFromRect:rect inView:self.view animated:YES];
        
    } else {
        // Opens an AlertView as sharing result when the Document Interaction Controller gets dismissed
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Instagram not found" message:@"Please install Instagram on your device!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
     //[self setupToolWithToolInfo:[self.toolsArray  objectAtIndex:kTextType]];
    
}


- (IBAction)settingBtnClicked:(id)sender
{
    SettingsViewController *settingVC = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
    [self.navigationController pushViewController:settingVC animated:YES];
    
}





#pragma mark-
#pragma mark MergeWatermark

-(void)setWaterMarkFrame
{
    //CGRect frme1 = _imageView.frame;
    CGRect frm = CGRectMake(0, 0, _imageView.image.size.width, _imageView.image.size.height);
    CGRect imageFrm = [Utility getUpdatedFrame:_imageView.image with:_imageView withActualFrame:frm];
    
    CGRect watermarkIFrm = watermarkImageView.frame;
    watermarkIFrm.origin.y = (imageFrm.origin.y + imageFrm.size.height) - watermarkImageView.frame.size.height;
    watermarkIFrm.origin.x = (imageFrm.size.width + imageFrm.origin.x) - watermarkImageView.frame.size.width;
    watermarkImageView.frame = watermarkIFrm;

}

-(UIImage *)layerViewFirstLayer:(UIImage *)firstImage SecongLayer:(UIImage *)secondImage secondFrame:(CGRect)secondFrame
{
    
    CGImageRef firstImageRef = firstImage.CGImage;
    CGFloat firstWidth = CGImageGetWidth(firstImageRef);
    CGFloat firstHeight = CGImageGetHeight(firstImageRef);
    
    CGSize mergedSize = CGSizeMake(firstWidth, firstHeight);
    UIGraphicsBeginImageContextWithOptions(mergedSize, NO, 1.0);
    
    [firstImage drawInRect:CGRectMake(0, 0, firstWidth, firstHeight)];
    [secondImage drawInRect:secondFrame];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    NSData *imageData = UIImagePNGRepresentation(newImage);
    
    UIImage *layeredImage = [UIImage imageWithData:imageData];
    
    return layeredImage;
    
}

#pragma mark-
#pragma mark BannerViews Methods-


- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    [self.banner setHidden:NO];
}


- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    [self.banner setHidden:YES];
}

#pragma mark - 
#pragma mark CANCEL BUTTON METHOD ===================

- (void)resetImageViewFrame  {
    //CGSize size = (_imageView.image) ? _imageView.image.size : _imageView.frame.size;
    
   /* if(size.width > 0 && size.height > 0) {
        CGFloat ratio = MIN(_scrollView.frame.size.width / size.width, _scrollView.frame.size.height / size.height);
        CGFloat W = ratio * size.width * _scrollView.zoomScale;
        CGFloat H = ratio * size.height * _scrollView.zoomScale;
        _imageView.frame = CGRectMake((_scrollView.width-W)/2, (_scrollView.height-H)/2, W, H);
    }*/
    
}

- (void)refreshImageView  {
    
    [self resetImageViewFrame];
    //[self resetZoomScaleWithAnimate:NO];
}




#pragma mark - 
#pragma mark DONE BUTTON METHOD ======================

- (IBAction)pushedDoneBtn:(id)sender
{
    self.view.userInteractionEnabled = NO;
    
    
    [self.currentTool executeWithCompletionBlock:^(UIImage *image, NSError *error, NSDictionary *userInfo) {
        if(error){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else if(image){
            self.rawImage = image;
            _imageView.image = image;
            
            [self resetImageViewFrame];
            self.currentTool = nil;
        }
        self.view.userInteractionEnabled = YES;
        
        // Recalls the banners to get to the bottom of the Toolbar MenuView
       // [NSTimer scheduledTimerWithTimeInterval:0.5  target:self selector:@selector(resetBanners)  userInfo:nil repeats:NO];
        
    }];
}


- (IBAction)pushedCancelBtn:(id)sender
{
    _imageView.image = self.rawImage;
    [self resetImageViewFrame];
    
    self.currentTool = nil;
    
    
    // Recalls the banners to get to the bottom of the Toolbar MenuView
    //[NSTimer scheduledTimerWithTimeInterval:0.5  target:self selector:@selector(resetBanners)  userInfo:nil repeats:NO];
}


- (void)pushedCloseBtn:(id)sender  {
    
    
    /*if(self.targetImageView==nil){
        if([self.delegate respondsToSelector:@selector(imageEditorDidCancel:)]){
            [self.delegate imageEditorDidCancel:self];
            
        }else{
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        
    }else{
        
        _imageView.image = self.targetImageView.image;
        [self restoreImageView:YES];
    }*/
    
}

- (void)pushedFinishBtn:(id)sender
{
   /* if(self.targetImageView==nil){
        if([self.delegate respondsToSelector:@selector(imageEditor:didFinishEdittingWithImage:)]){
            [self.delegate imageEditor:self didFinishEdittingWithImage:_originalImage];
            
        }else{
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        
    } else {
        
        _imageView.image = _originalImage;
        [self restoreImageView:NO];
    }*/
}

#pragma mark-
#pragma mark GADBannerViewDelegate

-(void)adViewDidReceiveAd:(GADBannerView *)bannerView{
    NSLog(@"RECEIVED");

    if (!self.adBannerViewIsVisible)
    {
        [UIView beginAnimations:@"animateAdBannerOn" context:NULL];
        [UIView commitAnimations];
        self.adBannerViewIsVisible = YES;
    }
    [self createScreenwideAd];
}

-(void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    if (self.adBannerViewIsVisible)
    {
        [UIView beginAnimations:@"animateAdBannerOff" context:NULL];
        [UIView commitAnimations];
        self.adBannerViewIsVisible = NO;
    }
}

-(void)createScreenwideAd
{
    if([adBannerView isHidden])
        return;
    interstitial = [[GADInterstitial alloc] init];
    interstitial.adUnitID = @"ca-app-pub-5250496564163769/7741378330";
    [interstitial setDelegate:self];
    GADRequest *request = [GADRequest request];
    NSString *uniqueString= [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    request.testDevices = [NSArray arrayWithObjects: @"MY_SIMULATOR_IDENTIFIER",uniqueString, nil];
    [interstitial loadRequest:request];
}

- (void)interstitialDidReceiveAd:(GADInterstitial *)ad
{

    [interstitial presentFromRootViewController: self];
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error
{
    interstitial = nil;
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)ad
{
    interstitial = nil;
}



@end
