//
//  HomeScreenViewController.h
//  PhotoFault
//
//  Created by Lekha Mishra on 29/05/15.
//  Copyright (c) 2015 Lekha Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface HomeScreenViewController : UIViewController <SlideNavigationControllerDelegate>
{
    IBOutlet UIImageView *homeImageView;//nishant
    IBOutlet UIButton *startBtn;//nishant
    
}


-(IBAction)goToHomeViewController:(id)sender;


@end
