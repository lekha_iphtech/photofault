//
//  SettingsViewController.h
//  PhotoFault
//
//  Created by Lekha Mishra on 20/05/15.
//  Copyright (c) 2015 Lekha Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "SettingCell.h"
#import "HTStoreManager.h"


@interface SettingsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,MFMailComposeViewControllerDelegate, HTStoreKitDelegate>
{
    NSArray *PurchaseIsArray;
    
    HTStoreManager *storeManager;
    UIImage *outPutImage;
    NSMutableDictionary *purchaseObjDic;
    
    
    NSMutableArray *headerTitleAray;

    IBOutlet UILabel *photoFaultLbl;//nishant

}
- (IBAction)backBtnclicked:(id)sender;//nishant
@property (retain, nonatomic) IBOutlet UITableViewCell *logoCell;
@property (retain, nonatomic) IBOutlet UIView *navigationView;

@property (strong, nonatomic) IBOutlet UITableView *settingsTableView;


@end
