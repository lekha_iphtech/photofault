//
//  EditViewController.h
//  PhotoFault
//
//  Created by Lekha Mishra on 20/05/15.
//  Copyright (c) 2015 Lekha Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
#import <GoogleMobileAds/GoogleMobileAds.h>//nishant
#import <AdSupport/AdSupport.h>//nishant

typedef enum EditingTools
{
    kFilterType,
    kEffectType,
    kBlurType,
    kRotateType,
    kCropType,
    kToneType,
    kResizeType,
    kFrameType,
    kStickerType,
    kTextType
    
} EditingTools;

@interface EditViewController : UIViewController <UIDocumentInteractionControllerDelegate, ADBannerViewDelegate, GADBannerViewDelegate, GADInterstitialDelegate>
{
    IBOutlet UIButton *slideFilterPanelBtn; 

  
    IBOutlet UIImageView *backImgView;
    
    UIImage *outPutImage;
  
    UIDocumentInteractionController *docInteracCont;
    UIImageView *watermark;
    GADInterstitial *interstitial;//nishant
    
    
    int count;
    
}

@property CGSize originalImageSize;//Vish

@property (nonatomic, strong) UIImage *rawImage;

@property (strong, nonatomic) IBOutlet UIImageView *watermarkImageView;
@property (strong, nonatomic) IBOutlet GADBannerView *adBannerView;//nishant
@property (nonatomic, strong) IBOutlet ADBannerView *banner;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UIView *menuView;
@property (nonatomic, strong) IBOutlet UIView *topBar;
@property (nonatomic, strong) IBOutlet UIView *topConfirmationBar;
@property (nonatomic, strong) IBOutlet UIScrollView *bottomMenuScrollView;

@property (nonatomic, strong) NSArray *filtersArray;

@property (nonatomic,assign) BOOL adBannerViewIsVisible;//nishant
@property (nonatomic,assign) BOOL bannerIsVisible;

-(IBAction)filtersBtnClicked:(id)sender;
-(IBAction)fxBtnClicked:(id)sender;
-(IBAction)ShareBtnClicked:(id)sender;
-(IBAction)cropBtnClicked:(id)sender;
-(IBAction)saveToPhotoLibrary:(id)sender;
-(IBAction)cancelButtonClicked:(id)sender;
-(IBAction)doneButtonClicked:(id)sender;
-(IBAction)backButtonClickedLibrary:(id)sender;
-(IBAction)rotateButtonClicked:(id)sender;
-(IBAction)toneCurveButtonClicked:(id)sender;
-(IBAction)blurAndFocusButtonClicked:(id)sender;
-(IBAction)settingBtnClicked:(id)sender;//devesh


-(void)menuItemDidSelected:(NSDictionary *)obj;
-(NSArray *)setUpTools;//Lekha
-(void)calculateInterstitialAdsCounts;

@end
