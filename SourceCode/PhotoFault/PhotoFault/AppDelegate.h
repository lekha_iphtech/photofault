//
//  AppDelegate.h
//  PhotoFault
//
//  Created by Lekha Mishra on 15/05/15.
//  Copyright (c) 2015 Lekha Mishra. All rights reserved.
//

#define APP_ID 1011770410 //Todo

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "LeftPanelViewController.h"



@class HomeScreenViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate,UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) HomeScreenViewController *viewController;

@property (strong, nonatomic) NSMutableArray *arrayOfFilters;
@property (strong, nonatomic) NSMutableArray *filtersCategoryArray;



@end

