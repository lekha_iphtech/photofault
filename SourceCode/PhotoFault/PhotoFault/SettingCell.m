//
//  SettingCell.m
//  PhotoFault
//
//  Created by iPHTech1 on 19/06/15.
//  Copyright (c) 2015 Lekha Mishra. All rights reserved.
//


/*
 *This class is created for settingViewController to
 *     show various cells in different sections.
 */


#import "SettingCell.h"

@implementation SettingCell
//@synthesize photoFaultLbl;
@synthesize directionLbl;
@synthesize photoPhaultImgView;

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
