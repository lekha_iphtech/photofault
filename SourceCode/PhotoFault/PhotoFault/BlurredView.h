//
//  BlurredView.h
//  PhotoFault
//
//  Created by Lekha Mishra on 28/05/15.
//  Copyright (c) 2015 Lekha Mishra. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface BlurredView : NSObject

+ (UIImage*) blur:(UIImage*)theImage;
+ (UIImage*) scaleIfNeeded:(CGImageRef)cgimg;
- (UIImage*) reOrientIfNeeded:(UIImage*)theImage;

@end
