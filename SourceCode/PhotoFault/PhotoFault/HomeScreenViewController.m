//
//  HomeScreenViewController.m
//  PhotoFault
//
//  Created by Lekha Mishra on 29/05/15.
//  Copyright (c) 2015 Lekha Mishra. All rights reserved.
//

#import "HomeScreenViewController.h"
#import "CenterViewController.h"
int timercount = 0;

@interface HomeScreenViewController ()

@end

@implementation HomeScreenViewController

- (void)viewDidLoad {
    
    
    
    //nishant start
    NSTimer *timer = [NSTimer timerWithTimeInterval:5.0 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    [timer fire];
    //nishant ends
    [super viewDidLoad];
    
}
#pragma mark - Custom methods (Nishant)
-(void)onTimer//nishant
{
    timercount = timercount + 1;
    if ( timercount == 1)
    {
    }
    else
    {
        [self animateImages];
    }
}
- (void)animateImages//nishant
{
    static int count = 0;
    NSArray *animationImages = @[[UIImage imageNamed:@"homeImage1"],[UIImage imageNamed:@"homeImage2"],[UIImage imageNamed:@"homeImage3"],[UIImage imageNamed:@"home"],[UIImage imageNamed:@"homeImage1"],[UIImage imageNamed:@"homeImage2"]];
    UIImage *image = [animationImages objectAtIndex:(count % [animationImages count])];
    
    [UIView transitionWithView:homeImageView
                      duration:3.0f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        // [self.view bringSubviewToFront:startBtn];
                        homeImageView.image = image;
                    } completion:^(BOOL finished) {
                        // [self.view bringSubviewToFront:startBtn];
                        count++;
                    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark-
#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return NO;
}

#pragma mark-
#pragma mark - IBActions Methods -

-(IBAction)goToHomeViewController:(id)sender
{
    
    CenterViewController *centerViewController = [[CenterViewController alloc] initWithNibName:@"CenterViewController" bundle:nil];
    [[SlideNavigationController sharedInstance] pushViewController:centerViewController animated:YES];
    
}


@end
