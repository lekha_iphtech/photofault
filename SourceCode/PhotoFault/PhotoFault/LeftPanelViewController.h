//
//  LeftPanelViewController.h
//  SlideoutNavigation
//
//  Created by Tammy Coron on 1/10/13.
//  Copyright (c) 2013 Tammy L Coron. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Filter;

@protocol LeftPanelViewControllerDelegate <NSObject>

@optional
- (void)imageSelected:(UIImage *)image withTitle:(NSString *)imageTitle withCreator:(NSString *)imageCreator;

@required
- (void)filterSelected:(Filter *)filter;

@end

@interface LeftPanelViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITableView *myTableView;
@property (nonatomic, assign) id<LeftPanelViewControllerDelegate> delegate;

-(void)reloadData;
-(void)selectNextItem;

@end