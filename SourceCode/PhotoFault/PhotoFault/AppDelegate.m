//
//  AppDelegate.m
//  PhotoFault
//
//  Created by Lekha Mishra on 15/05/15.
//  Copyright (c) 2015 Lekha Mishra. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeScreenViewController.h"
#import "Filter.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOption
{
    // Override point for customization after application launch.
    
    [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:@"count"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.viewController = [[HomeScreenViewController alloc] initWithNibName:@"HomeScreenViewController" bundle:nil];
    
    self.arrayOfFilters = [[NSMutableArray alloc] init];
    
   /* UINavigationController *navController  = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    [navController.navigationBar setTintColor:[UIColor redColor]];*/
    
    
    LeftPanelViewController *leftMenu = [[LeftPanelViewController alloc] initWithNibName:@"LeftPanelViewController" bundle:nil];
    
    SlideNavigationController *slideNavigationController = [[SlideNavigationController alloc] initWithRootViewController:self.viewController];
    [SlideNavigationController sharedInstance].leftMenu = leftMenu;
    [SlideNavigationController sharedInstance].menuRevealAnimationDuration = .18;
    
    
    [[NSNotificationCenter defaultCenter] addObserverForName:SlideNavigationControllerDidClose object:nil queue:nil usingBlock:^(NSNotification *note) {
        NSString *menu = note.userInfo[@"menu"];
        NSLog(@"Closed %@", menu);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:SlideNavigationControllerDidOpen object:nil queue:nil usingBlock:^(NSNotification *note) {
        NSString *menu = note.userInfo[@"menu"];
        NSLog(@"Opened %@", menu);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:SlideNavigationControllerDidReveal object:nil queue:nil usingBlock:^(NSNotification *note) {
        NSString *menu = note.userInfo[@"menu"];
        NSLog(@"Revealed %@", menu);
    }];

    
    self.window.rootViewController = slideNavigationController;
    
    [self.window makeKeyAndVisible];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    int appOpenCount = 1;
    
    NSString *count = [[NSUserDefaults standardUserDefaults] objectForKey:@"count"];
    NSString *isRated = [[NSUserDefaults standardUserDefaults] objectForKey:@"isRated"];
    if(!isRated || [isRated isEqualToString:@"No"])
    {
        if(count)
        {
            appOpenCount = [count intValue];
            
            if(appOpenCount == 5)
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"If you enjoy using Photofault app,would you mind taking a moment to rate it? Thanks for your support!" message:nil delegate:self cancelButtonTitle:@"Rate Photofault" otherButtonTitles:@"Remind me latter",@"No, thanks", nil];
                [alert show];
            
            }
            else
            {
                appOpenCount++;
                NSString *count = [NSString stringWithFormat:@"%d",appOpenCount];
                [[NSUserDefaults standardUserDefaults] setObject:count forKey:@"count"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }

        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"count"];
            [[NSUserDefaults standardUserDefaults] synchronize];

        }
    }
    
  }


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"No" forKey:@"isRated"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else if (buttonIndex == 2)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"Yes" forKey:@"isRated"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else if (buttonIndex == 0)
    {
        NSString *linkToReview = [NSString stringWithFormat:@"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%d", APP_ID];
        
        float ver_float = [[[UIDevice currentDevice] systemVersion] floatValue];
        if(ver_float >= 7.0)
        {
            linkToReview = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%d", APP_ID];
        }
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:linkToReview]];
        [[NSUserDefaults standardUserDefaults] setObject:@"Yes" forKey:@"isRated"];
        [[NSUserDefaults standardUserDefaults] synchronize];

    }
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"count"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
