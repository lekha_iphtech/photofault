
//  SlideoutNavigation
//
//  Created by Tammy Coron on 1/10/13.
//  Copyright (c) 2013 Tammy L Coron. All rights reserved.
//

#import "GPUImage.h"

@interface Filter : NSObject


@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *filterName;
@property (nonatomic, copy) NSString *imageName;
@property (nonatomic, copy) NSString *categoryName;

@property (nonatomic, assign) NSInteger num;
@property (nonatomic, assign) BOOL isCameraEffect;

+ (GPUImageOutput<GPUImageInput> *)processFilterForNum:(int)no;

@end