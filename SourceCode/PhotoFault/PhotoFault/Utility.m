//
//  Utility.m
//  PhotoFault
//
//  Created by Lekha Mishra on 28/05/15.
//  Copyright (c) 2015 Lekha Mishra. All rights reserved.
//

#import "Utility.h"
#import <AudioToolbox/AudioToolbox.h>

@implementation Utility

#pragma mark-
#pragma mark Methods for click beeps

// Sound clips
+(void)playCamClick
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"camClick" ofType:@"wav"];
    SystemSoundID soundID;
    NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundID);
    AudioServicesPlaySystemSound(soundID);
}

+(void)playBeep
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"beep" ofType:@"mp3"];
    SystemSoundID soundID;
    NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)filePath, &soundID);
    AudioServicesPlaySystemSound(soundID);
}

+ (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

+(int)isIphone
{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenHeight = screenSize.height;
    if (screenHeight == 568) {
        return iPhone5;
    }
    else if(screenHeight == 667)
    {
        return iPhone6;
    }
    else if(screenHeight == 736)
    {
        return iPhone6Plus;
    }
    else
        return iPhone4;
    
}

//Vish Start

+(CGRect) getUpdatedFrame :(UIImage *)image with :(UIImageView *)imageView withActualFrame:(CGRect) actualImageFrame
{
    
    float imageWidth = image.size.width;
    float imageHeight = image.size.height;
    
    float imgViewWidth = imageView.frame.size.width;
    float imgViewHeight = imageView.frame.size.height;
    
    float xInActualImage = actualImageFrame.origin.x;
    float yInActualImage = actualImageFrame.origin.y;
    float widthInActualImage = actualImageFrame.size.width;
    float heightInActualImage = actualImageFrame.size.height;
    
    float updatedWidth;
    float updatedHeight;
    
    //If aspect fill then "<" if aspect fit than ">"
    
    if(imageWidth > imageHeight)
    {
        updatedWidth = imgViewWidth;
        updatedHeight = (imageHeight*imgViewWidth)/imageWidth;
        if(updatedHeight > imgViewHeight)
        {
            updatedHeight = imgViewHeight;
            updatedWidth = (imageWidth*imgViewHeight)/imageHeight;
        }
    }
    else
    {
        updatedHeight = imgViewHeight;
        updatedWidth = (imageWidth*imgViewHeight)/imageHeight;
        
        if(updatedWidth > imgViewWidth)
        {
            updatedWidth = imgViewWidth;
            updatedHeight = (imageHeight*imgViewWidth)/imageWidth;
        }
        
    }
    
    CGFloat ratio = updatedWidth/imageWidth;
    
    CGFloat xOffset = (imgViewWidth - updatedWidth)/2;
    CGFloat yOffset = (imgViewHeight - updatedHeight)/2;
    
    CGFloat xInImageView = xInActualImage*ratio + xOffset;
    CGFloat yInImageView = yInActualImage*ratio + yOffset;
    
    CGFloat widthOfFaceInImageView  = widthInActualImage*ratio;
    CGFloat heightOfFaceInImageView  = heightInActualImage*ratio;
    
    CGRect frame = CGRectMake(xInImageView, yInImageView, widthOfFaceInImageView, heightOfFaceInImageView);
    
    return frame;
    
}

//Vish End



@end
